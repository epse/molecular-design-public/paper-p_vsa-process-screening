# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

from feos.si import *
from feos.pcsaft import PcSaftParameters
from feos.eos import EquationOfState
from feos.dft import HelmholtzEnergyFunctional

class FluidMixture:
    """
    Class to define the PC-SAFT object of the feed mixture.

    Attributes
    -----------
    fluids: list of str
        List of the names of the fluids in the mixture.
    mole_fractions: list of float
        List of the mole fractions of the fluids in the mixture. Should add up to 1.
    path_pcsaft_parameters: str
        Path to the JSON file containing the PC-SAFT parameters of the fluids.
    ratio_heat_capacities: list of float
        List of the ratio of heat capacities of the fluids in the mixture.
    path_k_ij: str, optional
        Path to the JSON file containing the binary interaction parameters k_ij. Default is '' which means that k_ij is not used for the mixture.
    """


    def __init__(self, fluids, mole_fractions, path_pcsaft_parameters, ratio_heat_capacities, path_k_ij = ''):

        self.fluids = fluids
        self.ratio_heat_capacities = ratio_heat_capacities
        
        # check if mole fractions add up to 1 with a tolerance of +-1e-3
        if abs(sum(mole_fractions) - 1) > 1e-3:
            raise ValueError('Mole fractions do not add up to 1.')
        self.mole_fractions = mole_fractions

        if path_k_ij == '':
            self.pcsaft_parameters = PcSaftParameters.from_json(fluids, path_pcsaft_parameters)
        else:
            self.pcsaft_parameters = PcSaftParameters.from_json(fluids, path_pcsaft_parameters, binary_path = path_k_ij)
        self.func = HelmholtzEnergyFunctional.pcsaft(self.pcsaft_parameters)
        self.eos = EquationOfState.pcsaft(self.pcsaft_parameters)
        
        # create a list of the pure component PcSaftParameters and HelmholzEnergyFunctional objects
        self.pcsaft_parameters_pure = []
        self.func_pure = []
        self.eos_pure = []
        for i in range(len(self.fluids)):
            self.pcsaft_parameters_pure.append(PcSaftParameters.from_json([self.fluids[i]], path_pcsaft_parameters))
            self.func_pure.append(HelmholtzEnergyFunctional.pcsaft(self.pcsaft_parameters_pure[i]))
            self.eos_pure.append(EquationOfState.pcsaft(self.pcsaft_parameters_pure[i]))


    def update_mole_fractions(self, mole_fractions):
        """Update the mole fractions of the feed mixture."""

        ## Check if mole fractions add up to 1 with a tolerance of +-1e-3
        #if abs(sum(mole_fractions) - 1) > 1e-3:
        #    raise ValueError('Mole fractions do not add up to 1.')
        
        # if value of mole fraction is greater 1, set it to 1
        for i in range(len(mole_fractions)):
            if mole_fractions[i] > 1:
                mole_fractions[i] = 1.0
        # if value of mole fraction is smaller 0, set it to 0
        for i in range(len(mole_fractions)):
            if mole_fractions[i] < 0:
                mole_fractions[i] = 0.0

        self.mole_fractions = mole_fractions
        

    def calc_ratio_heat_capacities_mixture(self):
        """Calculate the ratio of heat capacities of the mixture."""

        c_p_mix = self.mole_fractions[0]*self.ratio_heat_capacities[0]/(self.ratio_heat_capacities[0]-1)*RGAS + self.mole_fractions[1]*self.ratio_heat_capacities[1]/(self.ratio_heat_capacities[1]-1)*RGAS
        c_v_mix = c_p_mix - RGAS
        self.ratio_heat_capacities_mixture = c_p_mix/c_v_mix
        