# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt






# path to results directory
path_results_directory = 'results/screening_loop/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70_20240119_171839'
#axis = 'purity'
axis = 'recovery'

# determine which process step is important for the calculation of the respective axis to know which error should be read
step_axis = {'purity': 'bd', 'recovery': 'rp'}

data = []
names = []

materials_to_drop = []

# loop over all csv-files in the results directory
for file_name in os.listdir(path_results_directory):
    if file_name.endswith('.csv') and file_name[0] in ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']:
        #print(file_name)
        path_file = path_results_directory + '/' + file_name
        df = pd.read_csv(path_file)
        df = df.drop(columns=['Unnamed: 0'])
        #df = df.sort_values(by=[axis], ascending=False)
        




        indices_to_drop_convergence = []
        for i in range(len(df)):

            # remove entries that have a purity or specific work of 0 or a purity larger than 1 or a too large error during solving of the model
            if df[axis][i] <= 0 or df[axis][i] > 1 or df['specific_work_in_kJ_kg'][i] <= 0 or df[f'max_error_balance_heavy_{step_axis[axis]}'][i] > 1 or df[f'max_error_balance_light_{step_axis[axis]}'][i] > 1 or df[f'max_error_balance_energy_{step_axis[axis]}'][i] > 1:
                indices_to_drop_convergence.append(i)
                #materials_to_drop.append(file_name[:-4])
                #print(f'{file_name[:-4]} dropped')
                #continue
                #break

        # drop material if all entries are dropped
        if len(indices_to_drop_convergence) == len(df):
            materials_to_drop.append(file_name[:-4])
            continue

        df = df.drop(indices_to_drop_convergence)

        # reassign indices so that they are consecutive
        df = df.reset_index(drop=True)

        #for i in range(len(df)):
#
        #    # check if purity and specific work are decreasing over pressure
        #    if i == 0:
        #        pass
        #    elif df[axis][i] > df[axis][i-1] or df['specific_work_in_kJ_kg'][i] > df['specific_work_in_kJ_kg'][i-1]:
        #        materials_to_drop.append(file_name[:-4])
        #        break


            

        if not file_name[:-4] in materials_to_drop:
            indices_to_drop_dominated = []
            for i in range(len(df)):
                #if i in indices_to_drop_convergence:
                #    continue
                
                # debuging
                if df[axis][i] >= 0.97:
                    print(f'{file_name[:-4]} has {axis} of {df[axis][i]}')

                # remove entries that are dominated by other entries
                for j in range(len(df)):
                    #if j in indices_to_drop_convergence:
                    #    continue
                    if i != j:
                        if df['specific_work_in_kJ_kg'][i] > df['specific_work_in_kJ_kg'][j] and df[axis][i] < df[axis][j]:
                            indices_to_drop_dominated.append(i)
                            print("DOMINATED")
                            break


            #df = df.drop(indices_to_drop_dominated)

            # reassign indices so that they are consecutive
            df = df.reset_index(drop=True)
        data.append(df)
        names.append(file_name[:-4])



# Create a figure and axis
fig, ax = plt.subplots()

#materials_to_drop = []
print(materials_to_drop)
counter = 0
# Loop over the data list and plot scatterplots
for df, name in zip(data, names):
    if name not in materials_to_drop:
        counter += 1
        print(f'{counter}: {name}')
        #ax.plot(df[axis], df['specific_work_in_kJ_kg'], label=name, marker='.', linestyle='-')
        ax.plot(df[axis], df['specific_work_in_kJ_kg'], label=name, linestyle='-')

#ax.vlines(0.98, 0, 1500, colors='r', linestyles='dashed')

# Set labels
ax.set_ylabel('specific work in kJ/kg')
ax.set_xlabel(axis)

# set bounds
ax.set_xlim([0.45, 1])
#ax.set_ylim([0, 1500])


# Add legend
#ax.legend()

# Show the plot
#plt.show()

# save figure
fig.savefig(f'{path_results_directory}/pareto_plot_{axis}.png', dpi=300, bbox_inches='tight')