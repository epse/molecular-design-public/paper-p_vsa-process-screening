# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


name_csv = "prisma-isotherm-simulated_rsm0011-4602_DFT128fit_max_purity_20240529_161321_sorted.csv"
directory_csv = "results/max_purity/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70_cement_20240529_161058"

fontsize = 11
figsize = (3.5, 3.0)

path_csv = f"{directory_csv}/{name_csv}"

# Read the CSV file
df = pd.read_csv(path_csv)

max_purities = df['max_purity']*100
ranking = np.linspace(1, len(df), len(df))

# set fonts for plots to latex fonts
plt.rcParams.update({"text.usetex": True, "pgf.texsystem": "pdflatex", 'font.family': 'sans-serif', 'font.sans-serif': 'Computer Modern Sans Serif', 'font.size': fontsize})

# create figure
fig, ax = plt.subplots(figsize=figsize)

# Plot the data
ax.plot(ranking, max_purities)
ax.set_xlabel('Ranking of materials')
ax.set_ylabel('Maximum possible purity in \%')


ax.set_ylim(0.0, 100.0)
ax.set_xlim(1, len(df))

#ax.set_xticks([100, 200, 300, 400, 500, 600, 700])

#horizontal red line at 0.9
ax.axhline(y=0.9*100, color='r', linestyle='-')

fig.savefig(f'{path_csv[:-4]}.pdf', bbox_inches='tight')
