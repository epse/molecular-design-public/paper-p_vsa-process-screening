# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering
"""
executable code for optimization of the PSA process model for a csv-file with 1D-DFT parameters of adsorbents on multiple threads
author: Fabian Mayer, Energy & Process Systems Engineering, ETH Zurich
date: 2023-11-15
"""

from feos.si import *
import pandas as pd
from datetime import datetime
import os


from Read_input import Read_input
from knitro_process_optimization import knitro_process_optimization
import multiprocessing



#%% Inputs

# input toml file for general parameters of the process valid for all adsorbents
input_file_toml = 'RSM0068_BUSQIQ_cement70.toml'

# input csv file for 1D-DFT parameters of adsorbents
input_file_csv = 'prisma-isotherm-simulated_rsm0011-4602_DFT128fit_max_purity_20240529_161321_sorted_cleaned_top44.csv'

path_input_directory = 'inputs/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70'
path_results_directory = 'results/screening_opt/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70_pdes_opt'
results_file_suffix = 'solution_p_des_opt_70'
path_bounds = 'inputs/optimization_input/bounds_general_test.toml'
path_optimization_input = 'inputs/optimization_input/knitro_nlp_test.opt'

# set to True if the respective variable should be optimized or to False if constant value from input_file should be used (for screening of adsorbents only p_des: True is meaningful)
variables = {'p_des': True,
            'sigma': False,
            'epsilon': False,
            'rho': False,
            'pore_size': False,
            'specific_surface_area': False}

min_purity = 0.9
min_recovery = 0.0

# number of cores to be used for parallelization
n_cores = 35

#%% End Inputs

#%% Input processing

# timestamp for result directory
timestamp_start = datetime.now().strftime("%Y%m%d_%H%M%S")

# create result directory
path_results_directory = f'{path_results_directory}_{timestamp_start}'
os.mkdir(path_results_directory)

path_input_file_toml = path_input_directory + '/' + input_file_toml
path_input_file_csv = path_input_directory + '/' + input_file_csv

# copy input files to result directory
os.system(f'cp {path_input_file_toml} {path_results_directory}/{input_file_toml}')
os.system(f'cp {path_input_file_csv} {path_results_directory}/{input_file_csv}')

# read input csv file
data_csv = pd.read_csv(path_input_file_csv)


# get indices of all entries in the csv file
indices = list(range(len(data_csv)))



#%% Run optimization
# loop over every entry in the csv file as one row belongs to one adsorbent
def process_adsorbent(i):
    timestamp_start_i = datetime.now().strftime("%Y%m%d_%H%M%S")
    print(f'MOF {i} of {len(indices)}')
    input_data_i = Read_input(path_input_file_toml, data_csv, i)
    adsorbent = input_data_i.create_adsorbent()
    process_conditions = input_data_i.create_process()
    feed_mixture = input_data_i.create_feed_mixture()

    result_data_i = []

    # create folder for results of current adsorbent
    path_results_directory_i = f'{path_results_directory}/{i}_{data_csv["MOF"][i]}_{data_csv["CCDC"][i]}_{timestamp_start_i}'
    os.mkdir(path_results_directory_i)

    #run optimization


    solution, specific_work, purity, recovery = knitro_process_optimization(path_optimization_input, process_conditions, adsorbent, feed_mixture, variables, path_bounds, path_results_directory_i, min_purity = min_purity, min_recovery = min_recovery)


    # add specific work and purity to solution dictionary
    solution['specific_work_in_kJ/kg'] = specific_work/(KILO*JOULE/KILOGRAM)
    solution['purity'] = purity
    solution['recovery'] = recovery

    timestamp_end = datetime.now().strftime("%Y%m%d_%H%M%S")

    # save solution dictionary to csv
    df_solution = pd.DataFrame.from_dict(solution, orient='index', columns=['value'])
    df_solution.to_csv(f'{path_results_directory_i}/{i}_{data_csv["MOF"][i]}_{data_csv["CCDC"][i]}{results_file_suffix}_{timestamp_end}.csv')


    # move knitro log file to result directory
    #os.system(f'mv knitro.log {path_results_directory}/knitro.log')


# Map the process_adsorbent function to each index in parallel
# Create a pool of worker processes with n_cores
pool = multiprocessing.Pool(processes=n_cores)

# Map the process_adsorbent function to each index in parallel
pool.map(process_adsorbent, indices)

# Close the pool and wait for all processes to finish
pool.close()
pool.join()



# %%
