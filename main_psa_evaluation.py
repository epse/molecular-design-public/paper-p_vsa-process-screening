# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering
"""
executable code for evaluation of the PSA process model
author: Fabian Mayer, Energy & Process Systems Engineering, ETH Zurich
date: 2023-11-09
"""

from feos.si import *

import pandas as pd
import numpy as np
from datetime import datetime
import os

from Read_input import Read_input
from psa_process_leastsquares import psa_process


#%% Inputs

input_file = 'RSM0068_ELUJEC95_cement70.toml'
path_input_directory = 'inputs/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70'
path_results_directory = 'results/evaluation_tests/ELUJECpuritytest_01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70'


#%% End Inputs
#%% Input processing

# timestamp for result directory
timestamp_start = datetime.now().strftime("%Y%m%d_%H%M%S")

# create result directory
path_results_directory = f'{path_results_directory}_{timestamp_start}'
os.mkdir(path_results_directory)

path_input_file = path_input_directory + '/' + input_file

# read input file
input_data = Read_input(path_input_file)


# create adsorbent object from input data
adsorbent = input_data.create_adsorbent()

# create process conditions object from input data
process_conditions = input_data.create_process()

# create feed mixture object from input data
feed_mixture = input_data.create_feed_mixture()



#%% Run process model

#df_process_results, df_blowdown, df_repressurization = psa_process(process_conditions, adsorbent, feed_mixture)
df_process_results, df_blowdown, df_repressurization = psa_process(process_conditions, adsorbent, feed_mixture, enthalpy_mean=False)

timestamp_end = datetime.now().strftime("%Y%m%d_%H%M%S")

# save dataframes to csv
df_process_results.to_csv(f'{path_results_directory}/{input_file[0:-5]}_df_process_results_{timestamp_end}.csv')
df_blowdown.to_csv(f'{path_results_directory}/{input_file[0:-5]}_df_blowdown.csv')
df_repressurization.to_csv(f'{path_results_directory}/{input_file[0:-5]}_df_repressurization.csv')

# save input file to result directory
os.system(f'cp {path_input_file} {path_results_directory}/{input_file}')