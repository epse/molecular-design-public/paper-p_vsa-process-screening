# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering
"""
executable code for evaluating the theoretical maximum purity of the PSA process model by Maring et al. 2013 for a csv-file with 1D-DFT parameters of adsorbents
author: Fabian Mayer, Energy & Process Systems Engineering, ETH Zurich
date: 2023-11-23
"""

from datetime import datetime
import os
import pandas as pd

from Read_input import Read_input
from get_name_properties import add_adsorbent_properties
from psa_process_max_purity import calc_max_purity


#%% Inputs

# input toml file for general parameters of the process valid for all adsorbents
input_file_toml = 'RSM1150_VIPYOK_cement70.toml'

# input csv file for 1D-DFT parameters of adsorbents
input_file_csv = 'prisma-isotherm-simulated_rsm0011-4602_DFT128fit.csv'

path_input_directory = 'inputs/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70'
path_results_directory = 'results/max_purity/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70_cement'

path_codes = 'inputs/properties_input/Codes.csv'
path_properties = 'inputs/properties_input/prisma-zeo++-simulated.csv'

#%% End Inputs

#%% Input processing

# timestamp for result directory
timestamp_start = datetime.now().strftime("%Y%m%d_%H%M%S")

# create result directory
path_results_directory = f'{path_results_directory}_{timestamp_start}'
os.mkdir(path_results_directory)

path_input_file_toml = path_input_directory + '/' + input_file_toml
path_input_file_csv = path_input_directory + '/' + input_file_csv

# read input csv file
data_csv = pd.read_csv(path_input_file_csv)

# add CCDC code, density, and heat capacity to dataframe of adsorbents
data_csv = add_adsorbent_properties(data_csv, path_codes, path_properties)

# get indices of all entries in the csv file
indices = list(range(len(data_csv)))

# loop over every second entry in the csv file as two rows belong to one adsorbent
for i in [j for j in indices if j % 2 == 0]:
    print(i)
    input_data_i = Read_input(path_input_file_toml, data_csv, i)
    adsorbent = input_data_i.create_adsorbent()
    process_conditions = input_data_i.create_process()
    feed_mixture = input_data_i.create_feed_mixture()

    try:
        max_purity_i = calc_max_purity(process_conditions, adsorbent, feed_mixture)
    except:
        max_purity_i = -1

    data_csv.loc[i, 'max_purity'] = max_purity_i

    # save dataframes to csv
    data_csv.to_csv(f'{path_results_directory}/{input_file_csv[0:-4]}_max_purity.csv')



# add timestamp to result file
timestamp_end = datetime.now().strftime("%Y%m%d_%H%M%S")
os.system(f'mv {path_results_directory}/{input_file_csv[0:-4]}_max_purity.csv {path_results_directory}/{input_file_csv[0:-4]}_max_purity_{timestamp_end}.csv')

# copy input toml file to result directory
os.system(f'cp {path_input_file_toml} {path_results_directory}/{input_file_toml}')

# %%
