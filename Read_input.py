# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

import toml
import pandas as pd
import numpy as np
from feos.si import *

from Adsorbent import Adsorbent
from ProcessConditions import ProcessConditions
from FluidMixture import FluidMixture

class Read_input:

    def __init__(self, path_input_file_toml, input_dataframe = pd.DataFrame(), row_csv = -1):
        """
        read input files and store raw input data
        
        Parameters
        ----------
        path_input_file_toml : str
            path to input file in toml format with either all process and adsorbent parameters or in case of screening of multiple adsorbents only the general process parameters valid for all adsorbents
        input_dataframe : pandas dataframe, optional
            pandas dataframe with 1D-DFT parameters of adsorbents, by default empty dataframe for the case that no screening is performed and all data is taken from the toml file
        row_csv : int, optional
            row number of the csv file to be read for the current adsorbent, by default -1 for the case that no screening is performed and all data is taken from the toml file
        """

        data = toml.load(path_input_file_toml)

        if not input_dataframe.empty:
            # overwrite 1D-DFT parameters from toml file with 1D-DFT parameters from dataframe
            dft_parameters = np.array(np.matrix(input_dataframe['DFT_parameters_fit'][row_csv])).ravel()
            data['adsorbent_parameters']['sigma'] = dft_parameters[0]
            data['adsorbent_parameters']['epsilon'] = dft_parameters[1]
            data['adsorbent_parameters']['pore_size'] = dft_parameters[2]
            data['adsorbent_parameters']['internal_surface'] = dft_parameters[3] 
            data['adsorbent_parameters']['pore_shape'] = input_dataframe['geometry'][row_csv]
            data['adsorbent_parameters']['rho_ads'] = input_dataframe['density_in_kg/m^3'][row_csv]
            data['adsorbent_parameters']['cp_adsorb'] = input_dataframe['cp_adsorb_in_J/kgK'][row_csv]
            data['adsorbent_parameters']['POAVF'] = input_dataframe['POAVF'][row_csv]
            data['identifier']['RSM'] = input_dataframe['MOF'][row_csv]
            data['identifier']['CCDC'] = input_dataframe['CCDC'][row_csv]


        self.raw_input_data = data


    def create_adsorbent(self):
        """create an Adsorbent object from the raw input data"""

        name = f"{self.raw_input_data['identifier']['RSM']}_{self.raw_input_data['identifier']['CCDC']}"
        geometry = self.raw_input_data['adsorbent_parameters']['pore_shape']
        pore_size = self.raw_input_data['adsorbent_parameters']['pore_size'] * eval(self.raw_input_data['adsorbent_parameters']['pore_size_unit'])
        sigma = self.raw_input_data['adsorbent_parameters']['sigma']
        epsilon = self.raw_input_data['adsorbent_parameters']['epsilon']
        rho = self.raw_input_data['adsorbent_parameters']['rho']
        specific_surface_area = self.raw_input_data['adsorbent_parameters']['internal_surface'] * eval(self.raw_input_data['adsorbent_parameters']['internal_surface_unit'])
        cp = self.raw_input_data['adsorbent_parameters']['cp_adsorb'] * eval(self.raw_input_data['adsorbent_parameters']['cp_adsorb_unit'])
        density = self.raw_input_data['adsorbent_parameters']['rho_ads'] * eval(self.raw_input_data['adsorbent_parameters']['rho_ads_unit'])
        mass = self.raw_input_data['adsorbent_parameters']['m_adsorb'] * eval(self.raw_input_data['adsorbent_parameters']['m_adsorb_unit'])
        void_fraction_bed = self.raw_input_data['adsorbent_parameters']['void_fraction']
        void_fraction_pore = self.raw_input_data['adsorbent_parameters']['POAVF']


        adsorbent = Adsorbent(name, geometry, pore_size, sigma, epsilon, rho, specific_surface_area, cp, density, mass, void_fraction_bed, void_fraction_pore)
        adsorbent.lennard_jones93_potential()
        adsorbent.pore1D()

        return adsorbent
    

    def create_process(self):
        """create a ProcessConditions object from the raw input data"""

        temperature_feed = self.raw_input_data['isotherm_parameters']['temperature_0'] * eval(self.raw_input_data['isotherm_parameters']['temperature_unit'])
        pressure_feed = self.raw_input_data['isotherm_parameters']['pressure_high'] * eval(self.raw_input_data['isotherm_parameters']['pressure_high_unit'])
        pressure_desorption = self.raw_input_data['isotherm_parameters']['pressure_low'] * eval(self.raw_input_data['isotherm_parameters']['pressure_low_unit'])
        steps = self.raw_input_data['process_parameters']['steps']
        isentropic_efficiency = self.raw_input_data['materials']['eta']

        process_conditions = ProcessConditions(temperature_feed, pressure_feed, pressure_desorption, isentropic_efficiency, steps)

        return process_conditions
    

    def create_feed_mixture(self):
        """create a FeedMixture object from the raw input data"""

        fluids = self.raw_input_data['materials']['adsorbates']
        mole_fractions = self.raw_input_data['materials']['molefracs_adsorbates']
        path_pcsaft_parameters = self.raw_input_data['paths']['fluid_parameters']
        ratio_heat_capacities = [self.raw_input_data['materials']['k_a'], self.raw_input_data['materials']['k_b']]
        try:
            path_k_ij = self.raw_input_data['paths']['k_ij']
        except:
            path_k_ij = ''

        feed_mixture = FluidMixture(fluids, mole_fractions, path_pcsaft_parameters, ratio_heat_capacities, path_k_ij=path_k_ij)

        return feed_mixture