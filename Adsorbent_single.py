from feos.si import *
from feos.dft import ExternalPotential, Pore1D, Adsorption1D, Geometry, DFTSolver, State

import numpy as np


class Adsorbent:
    """
    Class to define an adsorbent by its 1D-DFT parameters.
    
    Attributes
    -----------
    name: str
        Name of the adsorbent.
    geometry: str
        Geometry of the adsorbent. Can be 'cartesian' or 'spherical' or 'cylindrical'.
    pore_size: SI float
        Pore size of the adsorbent.
    sigma: float
        sigma parameter of the 1D-DFT model.
    epsilon: float
        epsilon parameter of the 1D-DFT model.
    rho: float
        rho parameter of the 1D-DFT model.
    specific_surface_area: SI float
        Specific surface area of the adsorbent.
    c_p: SI float
        Heat capacity of the adsorbent.
    density: SI float
        Density of the adsorbent.
    mass: SI float
        Mass of the adsorbent.
    void_fraction: float
        Void fraction of the adsorbent.

    """

    def __init__(self, name, geometry, pore_size, sigma, epsilon, rho, specific_surface_area, cp, density, mass, void_fraction):

        self.name = name
        self.geometry = geometry
        self.pore_size = pore_size
        self.sigma = sigma
        self.epsilon = epsilon
        self.rho = rho
        self.specific_surface_area = specific_surface_area
        self.cp = cp
        self.density = density
        self.mass = mass
        self.void_fraction = void_fraction

        self.void_volume = self.mass/self.density * self.void_fraction

    
    def lennard_jones93_potential(self):
        """
        Create Lennard Jones potential for the adsorbent.
        """
        self.external_potential = ExternalPotential.LJ93(self.sigma, self.epsilon, self.rho)


    def pore1D(self):
        """
        Create 1D-DFT pore object for the adsorbent.
        """
        #self.pore = Pore1D(eval(f"Geometry.{self.geometry}"), self.pore_size, self.external_potential, n_grid = 1024)   
        self.pore = Pore1D(eval(f"Geometry.{self.geometry}"), self.pore_size, self.external_potential, n_grid = 128)   
        
    def update_specific_surface_area(self, specific_surface_area):
        """
        Update the specific surface area of the adsorbent.
        """
        self.specific_surface_area = specific_surface_area

    def update_dft_parameters(self, sigma, epsilon, rho, pore_size):
        """
        Update the DFT parameters of the adsorbent and external potential as well as the pore object.
        """
        self.sigma = sigma
        self.epsilon = epsilon
        self.rho = rho
        self.pore_size = pore_size

        self.lennard_jones93_potential()
        self.pore1D()



    def calc_profile(self, T, p, fluid_mixture, calc_enthalpy = False):
        """
        Calculate the profile of a fluid mixture at a certain temperature T and pressure p.
        """
        # specify solvers that are tried after the default solver fails
        solvers_str = ['DFTSolver().picard_iteration(max_iter=100, tol=1e-10, damping_coefficient=0.05).anderson_mixing()', 'DFTSolver().anderson_mixing().picard_iteration()', 'DFTSolver().picard_iteration().anderson_mixing()', 'DFTSolver().picard_iteration(max_iter=100, tol=1e-10, damping_coefficient=0.03).anderson_mixing()', 'DFTSolver().picard_iteration(max_iter=100, tol=1e-10, damping_coefficient=0.01).anderson_mixing()']
        
        if np.max(fluid_mixture.mole_fractions) >= 1.0 and calc_enthalpy:
            pure = True
            j_purecomponent = np.argmax(fluid_mixture.mole_fractions)
        else:
            pure = False
        
        try:
            if pure:
                profile = self.pore.initialize(State(fluid_mixture.func_pure[j_purecomponent], T, pressure=p)).solve()
            else:
                profile = self.pore.initialize(State(fluid_mixture.func, T, pressure=p, molefracs = np.array(fluid_mixture.mole_fractions))).solve()
            print('success with solver default')        
        except Exception as e:
            for solver_str_i in solvers_str:
                try:
                    solver = eval(solver_str_i)
                    if pure:
                        profile = self.pore.initialize(State(fluid_mixture.func_pure[j_purecomponent], T, pressure=p)).solve(solver=solver)
                    else:
                        profile = self.pore.initialize(State(fluid_mixture.func, T, pressure=p, molefracs = np.array(fluid_mixture.mole_fractions))).solve(solver=solver)
                    print(f'success with solver {solver_str_i}')
                    break
                except:
                    pass

            else:
                for j in range(10):
                    print(f'try with adjusted pressure {j}')
                    # Adjust p randomly between +-0.1%
                    p_rand = p * (1 + np.random.uniform(-0.001, 0.001))
                    for solver_str_i in solvers_str:
                        try:    
                            solver = eval(solver_str_i)
                            if pure:
                                profile = self.pore.initialize(State(fluid_mixture.func_pure[j_purecomponent], T, pressure=p_rand)).solve(solver=solver)
                            else:
                                profile = self.pore.initialize(State(fluid_mixture.func, T, pressure=p_rand, molefracs = np.array(fluid_mixture.mole_fractions))).solve(solver=solver)
                            print(f'success with adjusted pressure {j} and solver {solver_str_i}')
                            break
                        except:
                            pass
                    else:
                        continue
                    break
                else:
                    # if no solution is found after 10 tries, raise error
                    raise e
   
        return profile, pure


    def adsorbed_amount(self, T, p, fluid_mixture):
        """
        Calculate the uptake of a fluid mixture at a certain temperature T and pressure p using the single point method.
        """

        profile, _ = self.calc_profile(T, p, fluid_mixture, calc_enthalpy = False)

        n_adsorbed_per_pore = profile.moles


        # transfer unit of n_adsorbed from mol per pore to mol per kg adsorbent
        match self.geometry:
            case 'Spherical':
                n_adsorbed = n_adsorbed_per_pore/(4*np.pi*(self.pore_size)**2) * self.specific_surface_area
            case 'Cylindrical':
                n_adsorbed = n_adsorbed_per_pore/(2*np.pi*self.pore_size*1*ANGSTROM) * self.specific_surface_area
            case 'Cartesian':
                n_adsorbed = n_adsorbed_per_pore/(1*ANGSTROM*ANGSTROM) * self.specific_surface_area

        return n_adsorbed

    def enthalpy_of_adsorption(self, T, p, fluid_mixture):
        """
        Calculate the partial molar enthalpy of adsorption for a fluid mixture at a certain temperature T and pressure p using the single point method.
        """
        
        profile, pure = self.calc_profile(T, p, fluid_mixture, calc_enthalpy = True)
        
        if not pure:

            enthalpy_of_adsorption = profile.partial_molar_enthalpy_of_adsorption



        else:
            # if mole fraction of one component is 1, the isotherm object is calculated for the pure component to avoid bug in feos
            j_purecomponent = np.argmax(fluid_mixture.mole_fractions)

            enthalpy_of_adsorption = [None]*len(fluid_mixture.mole_fractions)
            for j in range(len(fluid_mixture.mole_fractions)):
                if j == j_purecomponent:
                    enthalpy_of_adsorption[j] = profile.enthalpy_of_adsorption
                else:
                    enthalpy_of_adsorption[j] = 0.0*JOULE/MOL


        
        return enthalpy_of_adsorption
    
    
