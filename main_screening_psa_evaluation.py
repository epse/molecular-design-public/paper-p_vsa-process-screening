# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering
"""
executable code for evaluating the PSA process model by Maring et al. 2013 for a csv-file with 1D-DFT parameters of adsorbents
author: Fabian Mayer, Energy & Process Systems Engineering, ETH Zurich
date: 2023-11-30
"""

from datetime import datetime
import os
import pandas as pd

from Read_input import Read_input
from get_name_properties import add_adsorbent_properties
from psa_process_leastsquares import psa_process


#%% Inputs

# input toml file for general parameters of the process valid for all adsorbents
input_file_toml = 'RSM0097_WIHDUN_cement70_debugging.toml'

# input csv file for 1D-DFT parameters of adsorbents
input_file_csv = 'prisma-isotherm-simulated_rsm0011-4602_DFT128fit_max_purity_20231213_094021_sorted_top47.csv'

path_input_directory = 'inputs/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70'
path_results_directory = 'results/screening/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70'

path_codes = 'inputs/properties_input/Codes.csv'
path_properties = 'inputs/properties_input/prisma-zeo++-simulated.csv'

# determine if full cycle should be calculated or only blowdown. If False, recovery can't be calculated
full_cycle = True

#%% End Inputs

#%% Input processing

# timestamp for result directory
timestamp_start = datetime.now().strftime("%Y%m%d_%H%M%S")

# create result directory
path_results_directory = f'{path_results_directory}_{timestamp_start}'
os.mkdir(path_results_directory)

path_input_file_toml = path_input_directory + '/' + input_file_toml
path_input_file_csv = path_input_directory + '/' + input_file_csv

# read input csv file
data_csv = pd.read_csv(path_input_file_csv)

# add CCDC code, density, and heat capacity to dataframe of adsorbents
data_csv = add_adsorbent_properties(data_csv, path_codes, path_properties)

# get indices of all entries in the csv file
indices = list(range(len(data_csv)))

# loop over every second entry in the csv file as two rows belong to one adsorbent
#for i in [j for j in indices if j % 2 == 0]:
for i in indices:
    timestamp_start_i = datetime.now().strftime("%Y%m%d_%H%M%S")
    print(i)
    input_data_i = Read_input(path_input_file_toml, data_csv, i)
    adsorbent = input_data_i.create_adsorbent()
    process_conditions = input_data_i.create_process()
    feed_mixture = input_data_i.create_feed_mixture()

    # run process model
    try:
        df_process_results, df_blowdown, df_repressurization = psa_process(process_conditions, adsorbent, feed_mixture, full_cycle=full_cycle)
        success = True
        # get specific work
        specific_work_in_kJ_kg = df_process_results['specific_work_in_kJ/kg'].iloc[-1]
        # get purity
        purity = df_process_results['purity'].iloc[-1]
        if full_cycle:
            # get recovery
            recovery = df_process_results['recovery'].iloc[-1]
        # get working capacity
        working_capacity_in_mol_kg = df_process_results['working_capacity_in_mol/kg'].iloc[-1]

    except:
        success = False
        df_process_results = pd.DataFrame()
        df_blowdown = pd.DataFrame()
        df_repressurization = pd.DataFrame()
        specific_work_in_kJ_kg = -1
        purity = -1
        recovery = -1
        working_capacity_in_mol_kg = -1

    data_csv.loc[i, 'specific_work_in_kJ/kg'] = specific_work_in_kJ_kg
    data_csv.loc[i, 'purity'] = purity
    if full_cycle:
        data_csv.loc[i, 'recovery'] = recovery
    data_csv.loc[i, 'working_capacity_in_mol/kg'] = working_capacity_in_mol_kg
    
    timestamp_end_i = datetime.now().strftime("%Y%m%d_%H%M%S")
    # calculate runtime
    runtime = datetime.strptime(timestamp_end_i, "%Y%m%d_%H%M%S") - datetime.strptime(timestamp_start_i, "%Y%m%d_%H%M%S")
    data_csv.loc[i, 'time_start'] = timestamp_start_i
    data_csv.loc[i, 'time_end'] = timestamp_end_i
    data_csv.loc[i, 'runtime_in_s'] = runtime.total_seconds()


    # save dataframes to csv
    data_csv.to_csv(f'{path_results_directory}/{input_file_csv[0:-4]}_screening.csv')

    if success:
        # create folder for results of current adsorbent
        path_results_directory_i = f'{path_results_directory}/{data_csv["MOF"][i]}_{data_csv["CCDC"][i]}_{timestamp_end_i}'
        os.mkdir(path_results_directory_i)

        # save dataframes to csv
        df_process_results.to_csv(f'{path_results_directory_i}/{input_file_csv[0:-4]}_{data_csv["MOF"][i]}_{data_csv["CCDC"][i]}_df_process_results.csv')
        df_blowdown.to_csv(f'{path_results_directory_i}/{input_file_csv[0:-4]}_{data_csv["MOF"][i]}_{data_csv["CCDC"][i]}_df_blowdown.csv')
        df_repressurization.to_csv(f'{path_results_directory_i}/{input_file_csv[0:-4]}_{data_csv["MOF"][i]}_{data_csv["CCDC"][i]}_df_repressurization.csv')



# add timestamp to result file
timestamp_end = datetime.now().strftime("%Y%m%d_%H%M%S")
os.system(f'mv {path_results_directory}/{input_file_csv[0:-4]}_screening.csv {path_results_directory}/{input_file_csv[0:-4]}_screening_{timestamp_end}.csv')

# copy input file to result directory
os.system(f'cp {path_input_file_toml} {path_results_directory}/{input_file_toml}')



# %%
