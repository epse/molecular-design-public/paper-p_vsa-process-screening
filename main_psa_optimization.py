# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering
"""
executable code for optimization of the PSA process model
author: Fabian Mayer, Energy & Process Systems Engineering, ETH Zurich
date: 2023-11-15
"""

from feos.si import *
import pandas as pd
from datetime import datetime
import os


from Read_input import Read_input
from knitro_process_optimization import knitro_process_optimization




#%% Inputs

input_file = 'RSM1150_VIPYOK_cement70_imp.toml'
path_input_directory = 'inputs/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70'
path_results_directory = 'results/opt_tests/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70_pdes_opt'
results_file_suffix = 'solution_p_des_opt_70'
path_bounds = 'inputs/optimization_input/bounds_general_test.toml'
path_optimization_input = 'inputs/optimization_input/knitro_nlp_test.opt'

# set to True if the respective variable should be optimized or to False if constant value from input_file should be used
variables = {'p_des': True,
            'sigma': False,
            'epsilon': False,
            'rho': False,
            'pore_size': False,
            'specific_surface_area': False}

min_purity = 0.92
min_recovery = 0.0

#%% End Inputs

#%% Input processing

# timestamp for result directory
timestamp_start = datetime.now().strftime("%Y%m%d_%H%M%S")

# create result directory
path_results_directory = f'{path_results_directory}_{timestamp_start}'
os.mkdir(path_results_directory)

path_input_file = path_input_directory + '/' + input_file

# read input file
input_data = Read_input(path_input_file)


# create adsorbent object from input data
adsorbent = input_data.create_adsorbent()

# create process conditions object from input data
process_conditions = input_data.create_process()

# create feed mixture object from input data
feed_mixture = input_data.create_feed_mixture()



#%% Run optimization

solution, specific_work, purity, recovery = knitro_process_optimization(path_optimization_input, process_conditions, adsorbent, feed_mixture, variables, path_bounds, path_results_directory, min_purity = min_purity, min_recovery = min_recovery)

print(f'specific work: {specific_work}')
print(f'purity: {purity}')
print(f'recovery: {recovery}')

print('done')

# add specific work and purity and recovery to solution dictionary
solution['specific_work_in_kJ/kg'] = specific_work/(KILO*JOULE/KILOGRAM)
solution['purity'] = purity
solution['recovery'] = recovery

timestamp_end = datetime.now().strftime("%Y%m%d_%H%M%S")

# save solution dictionary to csv
df_solution = pd.DataFrame.from_dict(solution, orient='index', columns=['value'])
df_solution.to_csv(f'{path_results_directory}/{input_file[0:-5]}{results_file_suffix}_{timestamp_end}.csv')

# save input file to result directory
os.system(f'cp {path_input_file} {path_results_directory}/{input_file}')

# move knitro log file to result directory
#os.system(f'mv knitro.log {path_results_directory}/knitro.log')