# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

"""
function to add CCDC code, density, and heat capacity to a dataframe of adsorbents with rsm-identifiers
author: Fabian Mayer, Energy & Process Systems Engineering, ETH Zurich
date: 2023-11-23
"""

import pandas as pd

def add_adsorbent_properties(df_adsorbents, path_codes, path_adsorbent_properties):
    """add CCDC code, density, and heat capacity to a dataframe of adsorbents with rsm-identifiers"""
    
    # read csv file with adsorbent properties
    df_adsorbent_properties = pd.read_csv(path_adsorbent_properties)
    # get density and heat capacity
    df_adsorbents['density_in_kg/m^3'] = df_adsorbents['MOF'].map(df_adsorbent_properties.set_index('MOF')['Density [g/cm^3]'])*1000
    df_adsorbents['cp_adsorb_in_J/kgK'] = df_adsorbents['MOF'].map(df_adsorbent_properties.set_index('MOF')['Cp [J/g.K]'])*1000
    df_adsorbents['POAVF'] = df_adsorbents['MOF'].map(df_adsorbent_properties.set_index('MOF')['POAVF [-]'])

    # read csv file with CCDC codes
    df_codes = pd.read_csv(path_codes)
    # get CCDC code
    df_adsorbents['CCDC'] = df_adsorbents['MOF'].map(df_codes.set_index('cif.label')['cif_name_orig'])


    return df_adsorbents