# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

import os
import pandas as pd
import matplotlib.pyplot as plt

results_folder = "results/opt_eps_constraint/epspurity_BUSQIQvsNOSHIO"

fontsize = 11
figsize = (3.5, 3.0)

colors = {"NOSHIO": "#1f77b4", "ELUJEC": "#1f77b4",  "VIPYOK": "#ff7f0e", "NAXLII": "#2ca02c", "SISFEH": "#d62728", "WOWMEC": "#9467bd", "BUSQIQ": "#d62728"} # "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf"}

# set fonts for plots to latex fonts
plt.rcParams.update({"text.usetex": True, "pgf.texsystem": "pdflatex", 'font.family': 'sans-serif', 'font.sans-serif': 'Computer Modern Sans Serif', 'font.size': fontsize})

# create figure
fig, ax = plt.subplots(figsize=figsize)

# Loop over folders inside results folder
for folder in os.listdir(results_folder):
    folder_path = os.path.join(results_folder, folder)
    
    # Check if folder is a directory
    if os.path.isdir(folder_path):
        # Get csv file with ending "eps_constraint.csv" which contains the results of the epsilon-constraint optimization
        csv_file = os.path.join(folder_path, [file for file in os.listdir(folder_path) if file.endswith("eps_constraint.csv")][0])
        
        # Read data from csv file
        data = pd.read_csv(csv_file)
        
        # Plot data in a scatter plot
        #ax.scatter(data["purity"], data["specific_work_in_kJ/kg"], marker="x", label=csv_file.split("/")[-1][0:6], linewidths=0.5, s=5)
        ax.plot(data["purity"]*100, data["specific_work_in_kJ/kg"], linestyle='-', linewidth=0.3, marker="o", markersize=3, markeredgewidth=0.3, color=colors[csv_file.split("/")[-1][0:6]])
        ax.plot(data["recovery"]*100, data["specific_work_in_kJ/kg"], linestyle='--', linewidth=0.3, marker="X", markersize=3, markeredgewidth=0.3, color=colors[csv_file.split("/")[-1][0:6]])
        #ax.plot(data["recovery"], data["purity"], linestyle='-', linewidth=0.3, marker="x", markersize=3, markeredgewidth=0.3, label=csv_file.split("/")[-1][0:6])
        
        # plot empty line without markers for legend entry
        ax.plot([], [], linestyle='-', linewidth=2, color=colors[csv_file.split("/")[-1][0:6]], label=csv_file.split("/")[-1][0:6])


        

# plot empty line to create legend entry for purity and recovery
ax.plot([], [], linestyle='--', linewidth=0.3, marker="X", markersize=3, markeredgewidth=0.3, color="black", label="recovery")
ax.plot([], [], linestyle='-', linewidth=0.3, marker="o", markersize=3, markeredgewidth=0.3, color="black", label="purity")


ax.set_xlabel("recovery/purity in \%")
ax.set_ylabel("specific work in kJ/kg")

#ax.set_xlim(40, 100)
#ax.set_ylim(200, 1100)

# Add colorbar using the mappable object
#cbar = plt.colorbar(scatter)
#cbar.set_label('recovery in \%')

# Add legend to plot
ax.legend()

#add more ticks to the x-axis
#ax.set_xticks([40, 50, 60, 70, 80, 90, 100])
        
# Save plot to file
fig.savefig(os.path.join(results_folder, "eps_constraint_plot_purity_recovery.pdf"), bbox_inches='tight')