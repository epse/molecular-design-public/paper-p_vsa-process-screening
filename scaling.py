# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

"""
Functions to scale and rescale a x-vector to values between 0 and 1 according to bounds of the respective variables
author: Fabian Mayer, Energy & Process Systems Engineering, ETH Zurich
date: 2023-11-15
"""

from feos.si import *

import numpy as np


def scale_x(x, variables: dict, bounds: dict):
    """
    Function to scale a x-vector to values between 0 and 1 according to bounds of the respective variables

    Parameters
    ----------
    x: numpy array
        x-vector to be scaled
    variables: dict
        Dictionary containing booleans if the variables should be optimized.
    bounds: dict
        2-Dimensional dictionary containing the lower and upper bounds for the variables to be optimized. Keys of the first dimension need to correspond to keys of the dictionary variables. Second dimension corresponds to lower and upper bounds.

    Returns
    -------
    x_scaled: numpy array
        Scaled x-vector with values between 0 and 1.
    """

    x_scaled = np.zeros(len(x))
    i = 0
    for keys, values in variables.items():
        if values:
            x_scaled[i] = (x[i] - bounds[keys]["lower"]) / (bounds[keys]["upper"] - bounds[keys]["lower"])
            i += 1

    return x_scaled


def rescale_x(x_scaled, variables: dict, bounds: dict):
    """
    Function to rescale a x-vector with values between 0 and 1 according to bounds of the respective variables

    Parameters
    ----------
    x_scaled: numpy array
        x-vector to be rescaled
    variables: dict
        Dictionary containing booleans if the variables should be optimized.
    bounds: dict
        2-Dimensional dictionary containing the lower and upper bounds for the variables to be optimized. Keys of the first dimension need to correspond to keys of the dictionary variables. Second dimension corresponds to lower and upper bounds.

    Returns
    -------
    x: numpy array
        Rescaled x-vector.
    """

    x = np.zeros(len(x_scaled))
    i = 0
    for keys, values in variables.items():
        if values:
            x[i] = x_scaled[i] * (bounds[keys]["upper"] - bounds[keys]["lower"]) + bounds[keys]["lower"]
            i += 1

    return x


def read_bounds(path_bounds):
    """
    Function to read the bounds for the variables to be optimized from a toml file and return them as a dictionary

    Parameters
    ----------
    path_bounds: str
        Path to the toml file containing the bounds for the variables to be optimized.

    Returns
    -------
    bounds: dict
        2-Dimensional dictionary containing the lower and upper bounds for the variables to be optimized. Keys of the first dimension correspond to names of variables. Second dimension corresponds to lower and upper bounds.
    """

    import toml

    # read toml file
    bounds = toml.load(path_bounds)

    return bounds