# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering
"""
executable code for multiple evaluation of the PSA process model looping over a variable (p_des or 1D-DFT parameters)
author: Fabian Mayer, Energy & Process Systems Engineering, ETH Zurich
date: 2023-11-09
"""

from feos.si import *

import pandas as pd
import numpy as np
from datetime import datetime
import os

from Read_input import Read_input
from psa_process_variable_dependent import psa_process_variable_dependent
from scaling import read_bounds



#%% Inputs

input_file = 'RSM0068_BUSQIQ_cement70.toml'
path_input_directory = 'inputs/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70'
path_results_directory = 'results/loop_test/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_3geo_SSE_70'
path_bounds = 'inputs/optimization_input/bounds_loop_test.toml'

# variables to be updated
variables_to_be_updated = {'p_des': True, 'specific_surface_area': False, 'sigma': False, 'epsilon': False, 'rho': False, 'pore_size': False}
# number of iterations
n_loop = 20

#%% End Inputs
#%% Input processing

# timestamp for result directory
timestamp_start = datetime.now().strftime("%Y%m%d_%H%M%S")

# create result directory
path_results_directory = f'{path_results_directory}_{timestamp_start}'
os.mkdir(path_results_directory)

path_input_file = path_input_directory + '/' + input_file

# read input file
input_data = Read_input(path_input_file)


# create adsorbent object from input data
adsorbent = input_data.create_adsorbent()

# create process conditions object from input data
process_conditions = input_data.create_process()

# create feed mixture object from input data
feed_mixture = input_data.create_feed_mixture()

# read bounds
bounds = read_bounds(path_bounds)
x_lb = bounds['p_des']['lower']*PASCAL
x_ub = bounds['p_des']['upper']*PASCAL
x = SIArray1.linspace(x_lb, x_ub, n_loop)

#%% Run process model

result_data = []

for x_i in x:
    variables_values = {'p_des': x_i}

    specific_work_in_kJ_kg, purity, df_process_results, df_blowdown, df_repressurization = psa_process_variable_dependent(variables_values, variables_to_be_updated, process_conditions, adsorbent, feed_mixture, full_cycle=False)

    result_data.append({'p_des_in_Pa': x_i/PASCAL, 'specific_work_in_kJ_kg': specific_work_in_kJ_kg, 'purity': purity})

    df_results = pd.DataFrame(result_data)
    df_results.to_csv(f'{path_results_directory}/{input_file[0:-5]}_df_results.csv')


timestamp_end = datetime.now().strftime("%Y%m%d_%H%M%S")


# save input file to result directory
os.system(f'cp {path_input_file} {path_results_directory}/{input_file}')

# rename results file to include timestamp
os.rename(f'{path_results_directory}/{input_file[0:-5]}_df_results.csv', f'{path_results_directory}/{input_file[0:-5]}_df_results_{timestamp_end}.csv')