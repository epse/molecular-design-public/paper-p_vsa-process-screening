# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering
"""
function that optimizes the PSA process using the solver Knitro
author: Fabian Mayer, Energy & Process Systems Engineering, ETH Zurich
date: 2023-11-15
"""

from knitro import *
import numpy as np
import pandas as pd
import os
from feos.si import *

from psa_process_variable_dependent import psa_process_variable_dependent
from scaling import scale_x, rescale_x, read_bounds



def knitro_process_optimization(path_optimization_input, process_conditions, adsorbent, feed_mixture, variables: dict, path_bounds, path_results_directory, min_purity = 0.0, min_recovery = 0.0):
    """
    Function that optimizes the PSA process using the solver Knitro.

    Parameters
    ----------
    optimization_input_path: str
        Path to the optimization input file.
    process_conditions: ProcessConditions object
        Object containing the process conditions.
    adsorbent: Adsorbent object
        Object containing the adsorbent properties.
    feed_mixture: FeedMixture object
        Object containing the feed mixture properties.
    variables: dict
        Dictionary containing booleans if the variables should be optimized. Keys of the dictionary correspond to the list potential_variables and need to have the same order. If set to False, the constant value from the input file is used.
    path_bounds: str
        Path to a toml file containing the bounds for the variables to be optimized.
    path_results_directory: str
        Path to the directory where the results of the optimization steps should be saved.
    min_purity: float, optional
        Minimum purity used as constraint in the optimization. If both min_purity and min_recovery are not given or equal 0.0, an unconstrained optimization is performed
    min_recovery: float, optional
        Minimum recovery used as constraint in the optimization. If not given or equals 0.0, no constraint on recovery is set and repressurization is not calculated to save computation time.

    Returns
    -------
    solution: dict
        Dictionary containing the solution of the optimized variables.
    specific_work: SI float
        Specific work of the optimal PSA process.
    purity: float
        Purity of the optimal PSA process.
    recovery: float
        Recovery of the optimal PSA process if min_recovery is given, else 0.0
    """

    #%% READ INPUT

    bounds = read_bounds(path_bounds)

    
    #%% PREPARE DICTIONARIES

    # dictionary to save the results of the optimization to check if the same point was already evaluated
    results_dict = {}
    results_dict['x'] = {}
    results_dict['x_rounded'] = {}
    results_dict['purity'] = {}
    results_dict['recovery'] = {}

    # list of variables that can be chosen from for the optimization
    potential_variables = ["p_des", "sigma", "epsilon", "rho", "pore_size", "specific_surface_area"]
    potential_variables_units = {potential_variables[0]: {'si': 'PASCAL', 'short': 'Pa'},
                                potential_variables[1]: {'si': '1', 'short': '1'},
                                potential_variables[2]: {'si': '1', 'short': '1'},
                                potential_variables[3]: {'si': '1', 'short': '1'},
                                potential_variables[4]: {'si': 'ANGSTROM', 'short': 'Angstrom'},
                                potential_variables[5]: {'si': 'METER**2/KILOGRAM', 'short': 'm^2/kg'}}

    # create a dictionary with constant values for variables that should not be optimized and the initial values for variables that should be optimized
    constant_variables = {}
    for key, value in variables.items():
        if key == potential_variables[0]:
            constant_variables[key] = process_conditions.pressure_desorption
        elif key == potential_variables[1]:
            constant_variables[key] = adsorbent.sigma
        elif key == potential_variables[2]:
            constant_variables[key] = adsorbent.epsilon
        elif key == potential_variables[3]:
            constant_variables[key] = adsorbent.rho
        elif key == potential_variables[4]:
            constant_variables[key] = adsorbent.pore_size
        elif key == potential_variables[5]:
            constant_variables[key] = adsorbent.specific_surface_area
        else:
            raise Exception("Variable not found in potential_variables list.")
                

    def x_vector_to_dict(x, variables, constant_variables, potential_variables_units, use_units = True):
        """
        Function to transform a x-vector with variables to dictionary with all potential variables including the constant variables.
        """
        x_dict = {}
        i = 0
        for key, value in variables.items():
            if value:
                if use_units or potential_variables_units[key]['si'] == '1':
                    x_dict[key] = x[i] * eval(potential_variables_units[key]['si'])
                else:
                    x_dict[f'{key}_in_{potential_variables_units[key]['short']}'] = x[i]
                i += 1
            else:
                if use_units or potential_variables_units[key]['si'] == '1':
                    x_dict[key] = constant_variables[key]
                else:
                    x_dict[f'{key}_in_{potential_variables_units[key]['short']}'] = constant_variables[key]/eval(potential_variables_units[key]['si'])
        return x_dict
    
    def dict_to_x_vector(x_dict, variables, potential_variables_units):
        """
        Function to transform a dictionary with all potential variables including the constant variables to a unitless x-vector with only the variables to be optimized.
        """
        x = np.zeros(len([variables[key] for key in variables.keys() if variables[key]]))
        i = 0
        for key, value in variables.items():
            if value:
                x[i] = x_dict[key]/eval(potential_variables_units[key]['si'])
                i += 1
        return x




    #%% CALLBACK FUNCTION

    # objective function and constraints
    def callbackEvalF(kc, cb, evalRequest, evalResult, userParams):
        """
        Callback function is called in each iteration to evaluate the black-box process.
        """

        if evalRequest.type != KN_RC_EVALFC:
            print ("*** callbackEvalF incorrectly called with eval type %d" % evalRequest.type)
            return -1
        x = evalRequest.x
        print("Variables scaled:   ", x)

        #check if the same point was already evaluated
        if np.round(x, 6) in results_dict['x_rounded'].values():
            specific_work_in_kJ_kg = [i for i in results_dict['x_rounded'] if results_dict['x_rounded'][i]==np.round(x, 6)][0]
            purity = results_dict['purity'][specific_work_in_kJ_kg]
            recovery = results_dict['recovery'][specific_work_in_kJ_kg]
            print("Same point already evaluated, objective function value: " + str(specific_work_in_kJ_kg))
            evalResult.obj = specific_work_in_kJ_kg
            # set constraint on purity
            if min_purity > 0.0 or min_recovery > 0.0:
                const_purity = purity - min_purity
                evalResult.c[0] = const_purity
                if min_recovery > 0.0:
                    const_recovery = recovery - min_recovery
                    evalResult.c[1] = const_recovery
            return 0
        

        try:
            x_unscaled = rescale_x(x, variables, bounds)
            print("Variables unscaled: ", x_unscaled)

            x_dict = x_vector_to_dict(x_unscaled, variables, constant_variables, potential_variables_units, use_units = True)
            
            if min_recovery > 0.0:
                specific_work_in_kJ_kg, purity, df_process_results, _, _ = psa_process_variable_dependent(x_dict, variables, process_conditions, adsorbent, feed_mixture, full_cycle=True)
                recovery = df_process_results['recovery'].iloc[-1]
            else:
                specific_work_in_kJ_kg, purity, _, _, _ = psa_process_variable_dependent(x_dict, variables, process_conditions, adsorbent, feed_mixture, full_cycle=False)
                recovery = 0.0

            #just for debugging TODO: remove when not needed anymore
            #specific_work_in_kJ_kg = x_unscaled[0]**2
            #purity = x_unscaled[0]*9e-5


            if np.isnan(specific_work_in_kJ_kg):
                raise Exception("Objective function value is NaN, set to high value.")


        except:
            # set objective function value to high value in case scaling or process evaluation was not successfull
            specific_work_in_kJ_kg = 1e6
            purity = 0.0
            recovery = 0.0

        obj_fct_value = specific_work_in_kJ_kg

        print("Objective function value: " + str(obj_fct_value))

        # save results to dictionary to check in later iterations if the same point was already evaluated
        results_dict['x'][obj_fct_value] = x
        results_dict['x_rounded'][obj_fct_value] = np.round(x, 6)
        results_dict['purity'][obj_fct_value] = purity
        results_dict['recovery'][obj_fct_value] = recovery

        x_dict_unitless = x_vector_to_dict(x_unscaled, variables, constant_variables, potential_variables_units, use_units = False)

        # save results to csv
        # transform x_dict to dataframe
        df_results = pd.DataFrame.from_dict(x_dict_unitless, orient='index', columns=['value'])
        # add specific work and purity and recovery to dataframe
        df_results.loc['specific_work_in_kJ/kg'] = [specific_work_in_kJ_kg]
        df_results.loc['purity'] = [purity]
        df_results.loc['recovery'] = [recovery]

        df_results_transposed = df_results.transpose()
        if os.path.isfile(f'{path_results_directory}/optimization_steps.csv'):
            # if the file already exists, read it and append the new results
            df_results_transposed.to_csv(f'{path_results_directory}/optimization_steps.csv', mode='a', header=False)
        else:
            # if the file does not exist, create it and write the new results
            df_results_transposed.to_csv(f'{path_results_directory}/optimization_steps.csv')


        # set constraint on purity
        if min_purity > 0.0 or min_recovery > 0.0:
            const_purity = purity - min_purity
            evalResult.c[0] = const_purity  
          
            if min_recovery > 0.0:
                const_recovery = recovery - min_recovery
                evalResult.c[1] = const_recovery

        # pass objective function value to knitro
        evalResult.obj = obj_fct_value


        return 0


    #%% OPTIMIZATION

     # Create a new Knitro solver instance.
    try:
        kc = KN_new()
    except:
        print("Failed to find a valid license.")
        quit()


    # Read knitro.opt file to set options.
    KN_load_param_file(kc, path_optimization_input)


    # count how many entries in the variables dictionary are set to True
    n = 0
    for key, value in variables.items():
        if value:
            n += 1

    # Add variables
    KN_add_vars(kc, n)


    # Set upper and lower bounds to 0 and 1
    xLoBnds=[0.0 for i in range(n)]
    xUpBnds=[1.0 for i in range(n)]
    KN_set_var_lobnds(kc, xLoBnds=xLoBnds)
    KN_set_var_upbnds(kc, xUpBnds=xUpBnds)


    # Set initial x-vector 
    x_init_unscaled = dict_to_x_vector(constant_variables, variables, potential_variables_units)

    x_init = scale_x(x_init_unscaled, variables, bounds)
        
    KN_set_var_primal_init_values(kc, xInitVals=x_init)


    # Add constraints 
    if min_purity > 0.0 or min_recovery > 0.0:
        if min_recovery > 0.0:
            m = 2
        else:
            m = 1
        KN_add_cons(kc, m)
        # Set lower bounds 
        iAllCon = [i for i in range(m)]
        iEvalCon = [i for i in range(m)]
        lobounds = [0.0] * m 
        KN_set_con_lobnds(kc, indexCons=iAllCon, cLoBnds=lobounds)


    # Create callback
    if min_purity > 0.0 or min_recovery > 0.0:
        cb = KN_add_eval_callback(kc, evalObj = True, indexCons=iEvalCon, funcCallback = callbackEvalF)
    else:
        cb = KN_add_eval_callback(kc, evalObj = True, funcCallback = callbackEvalF)

    # Set direction of optimization.
    KN_set_obj_goal(kc, KN_OBJGOAL_MINIMIZE)

    # Solve the problem.
    nStatus = KN_solve(kc)

    # Obtaining solution information.
    nStatus, objSol, x, lambda_ = KN_get_solution(kc)
    tcpu = KN_get_solve_time_cpu(kc)
    treal = KN_get_solve_time_real(kc)
    
    if min_purity > 0.0 or min_recovery > 0.0:
        c = KN_get_con_values(kc)
        purity = c[0] + min_purity
        if min_recovery > 0.0:
            recovery = c[1] + min_recovery
        else:
            recovery = 0.0
    else:
        if np.round(x, 6) in results_dict['x_rounded'].values():
            specific_work_in_kJ_kg_temp = [i for i in results_dict['x_rounded'] if results_dict['x_rounded'][i]==np.round(x, 6)][0]
            purity = results_dict['purity'][specific_work_in_kJ_kg_temp]
            recovery = 0.0  #recovery not calculated in unconstrained optimization and therefore set to 0
        else:
            purity = 0.0    
            recovery = 0.0

    # Objective value evaluation
    print("Optimal objective value  = %e" % objSol)
    print("Total CPU time           = %f" % tcpu)
    print("Total real time          = %f" % treal)
    print("Optimal x")
    for i in range (n):
        print ("  x[%d] = %e " % (i, x[i]))
    print("  KKT optimality violation = %e" % KN_get_abs_opt_error (kc))
    print("  Feasibility violation    = %e" % KN_get_abs_feas_error (kc))

    # Delete the Knitro solver instance.
    KN_free (kc)


    specific_work = objSol*(KILO*JOULE/KILOGRAM)

    

    x_unscaled = rescale_x(x, variables, bounds)


    # create solution dictionary
    solution = {}
    solution = x_vector_to_dict(x_unscaled, variables, constant_variables, potential_variables_units, use_units = False)



    return solution, specific_work, purity, recovery
