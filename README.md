# Process-based screening of porous materials for vacuum swing adsorption based on 1D classical density functional theory and PC-SAFT

In this repository, you can find the Python code and result data for the paper "Process-based screening of porous materials for vacuum swing adsorption based on 1D classical density functional theory and PC-SAFT" by Fabian Mayer, Benedikt Buhk, Johannes Schilling, Philipp Rehner, Joachim Gross, and André Bardow.

For questions, please contact Fabian Mayer: fmayer@ethz.ch

All executable scripts have the prefix "main_". Details of what the scripts can be used for are given in the headers.