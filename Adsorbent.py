# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

from feos.si import *
from feos.dft import ExternalPotential, Pore1D, Adsorption1D, Geometry, DFTSolver

import numpy as np


class Adsorbent:
    """
    Class to define an adsorbent by its 1D-DFT parameters.
    
    Attributes
    -----------
    name: str
        Name of the adsorbent.
    geometry: str
        Geometry of the adsorbent. Can be 'cartesian' or 'spherical' or 'cylindrical'.
    pore_size: SI float
        Pore size of the adsorbent.
    sigma: float
        sigma parameter of the 1D-DFT model.
    epsilon: float
        epsilon parameter of the 1D-DFT model.
    rho: float
        rho parameter of the 1D-DFT model.
    specific_surface_area: SI float
        Specific surface area of the adsorbent.
    c_p: SI float
        Heat capacity of the adsorbent.
    density: SI float
        Density of the adsorbent.
    mass: SI float
        Mass of the adsorbent.
    void_fraction_bed: float
        Void fraction of the adsorbent bed.
    void_fraction_pore: float
        Void fraction of the adsorbent (also called POAVF).

    """

    def __init__(self, name, geometry, pore_size, sigma, epsilon, rho, specific_surface_area, cp, density, mass, void_fraction_bed, void_fraction_pore):

        self.name = name
        self.geometry = geometry
        self.pore_size = pore_size
        self.sigma = sigma
        self.epsilon = epsilon
        self.rho = rho
        self.specific_surface_area = specific_surface_area
        self.cp = cp
        self.density = density
        self.mass = mass
        self.void_fraction_bed = void_fraction_bed
        self.void_fraction_pore = void_fraction_pore

        self.void_volume_pore = self.mass/self.density * self.void_fraction_pore
        self.void_volume_bed = self.mass/self.density / (1-self.void_fraction_bed) - self.mass/self.density
        
        #self.void_volume = self.void_volume_pore + self.void_volume_bed  # use if adsorption model gives excess adsorption
        self.void_volume = self.void_volume_bed  # use if adsorption model gives absolute adsorption

    
    def lennard_jones93_potential(self):
        """
        Create Lennard Jones potential for the adsorbent.
        """
        self.external_potential = ExternalPotential.LJ93(self.sigma, self.epsilon, self.rho)


    def pore1D(self):
        """
        Create 1D-DFT pore object for the adsorbent.
        """
        #self.pore = Pore1D(eval(f"Geometry.{self.geometry}"), self.pore_size, self.external_potential, n_grid = 1024)
        self.pore = Pore1D(eval(f"Geometry.{self.geometry}"), self.pore_size, self.external_potential, n_grid = 128)
        #self.pore = Pore1D(eval(f"Geometry.{self.geometry}"), self.pore_size, self.external_potential, n_grid = 256)
        
    def update_specific_surface_area(self, specific_surface_area):
        """
        Update the specific surface area of the adsorbent.
        """
        self.specific_surface_area = specific_surface_area

    def update_dft_parameters(self, sigma, epsilon, rho, pore_size):
        """
        Update the DFT parameters of the adsorbent and external potential as well as the pore object.
        """
        self.sigma = sigma
        self.epsilon = epsilon
        self.rho = rho
        self.pore_size = pore_size

        self.lennard_jones93_potential()
        self.pore1D()


    def calc_dft_isotherm_object(self, T, p, fluid_mixture, calc_enthalpy = False):
        """
        Calculate the DFT isotherm object for the adsorption of a fluid mixture at a certain temperature T and pressure p.
        """
        # specify solvers that are tried after the default solver fails
        solvers_str = ['DFTSolver().picard_iteration(max_iter=100, tol=1e-10, damping_coefficient=0.05).anderson_mixing()', 'DFTSolver().anderson_mixing().picard_iteration()', 'DFTSolver().picard_iteration().anderson_mixing()', 'DFTSolver().picard_iteration(max_iter=100, tol=1e-10, damping_coefficient=0.03).anderson_mixing()', 'DFTSolver().picard_iteration(max_iter=100, tol=1e-10, damping_coefficient=0.01).anderson_mixing()', 'DFTSolver().newton().picard_iteration()']
        
        if np.max(fluid_mixture.mole_fractions) >= 1.0 and calc_enthalpy:
            pure = True
            j_purecomponent = np.argmax(fluid_mixture.mole_fractions)
        else:
            pure = False
        
        try:
            if pure:
                dft_isotherm = Adsorption1D.adsorption_isotherm(fluid_mixture.func_pure[j_purecomponent], T, SIArray1([p]), self.pore)
            else:
                dft_isotherm = Adsorption1D.adsorption_isotherm(fluid_mixture.func, T, SIArray1([p]), self.pore, molefracs = np.array(fluid_mixture.mole_fractions))
            if (np.any(np.isnan(dft_isotherm.adsorption/MOL)) and not calc_enthalpy) or (calc_enthalpy and np.any(np.isnan(dft_isotherm.partial_molar_enthalpy_of_adsorption/(JOULE/MOL)))):
                    raise Exception('nan')
            #print('success with solver default')        
        except Exception as e:
            for solver_str_i in solvers_str:
                try:
                    solver = eval(solver_str_i)
                    if pure:
                        dft_isotherm = Adsorption1D.adsorption_isotherm(fluid_mixture.func_pure[j_purecomponent], T, SIArray1([p]), self.pore, solver = solver)
                    else:
                        dft_isotherm = Adsorption1D.adsorption_isotherm(fluid_mixture.func, T, SIArray1([p]), self.pore, molefracs = np.array(fluid_mixture.mole_fractions), solver = solver)
                    if (np.any(np.isnan(dft_isotherm.adsorption/MOL)) and not calc_enthalpy) or (calc_enthalpy and np.any(np.isnan(dft_isotherm.partial_molar_enthalpy_of_adsorption/(JOULE/MOL)))): 
                        raise Exception('nan')
                    #print(f'success with solver {solver_str_i}')
                    break
                except:
                    pass
            else:
                for j in range(10):
                    #print(f'try with adjusted pressure {j}')
                    # Adjust p randomly between +-0.1%
                    p_rand = p * (1 + np.random.uniform(-0.001, 0.001))
                    for solver_str_i in solvers_str:
                        try:    
                            solver = eval(solver_str_i)
                            if pure:
                                dft_isotherm = Adsorption1D.adsorption_isotherm(fluid_mixture.func_pure[j_purecomponent], T, SIArray1([p_rand]), self.pore, solver = solver)
                            else:
                                dft_isotherm = Adsorption1D.adsorption_isotherm(fluid_mixture.func, T, SIArray1([p_rand]), self.pore, molefracs = np.array(fluid_mixture.mole_fractions), solver = solver)
                            if (np.any(np.isnan(dft_isotherm.adsorption/MOL)) and not calc_enthalpy) or (calc_enthalpy and np.any(np.isnan(dft_isotherm.partial_molar_enthalpy_of_adsorption/(JOULE/MOL)))):
                                raise Exception('nan')
                            #print(f'success with adjusted pressure {j} and solver {solver_str_i}')
                            break
                        except:
                            pass
                    else:
                        continue
                    break
                else:
                    # if no solution is found after 10 tries, raise error
                    raise e

        return dft_isotherm, pure
            
    
    
    def get_adsorbed_amount(self, dft_isotherm):
        """
        Get the uptake of a fluid mixture from a dft_isotherm object created at a certain temperature T and pressure p.
        """

        #dft_isotherm, _ = self.calc_dft_isotherm_object(T, p, fluid_mixture, calc_enthalpy = False)

        n_adsorbed_per_pore = dft_isotherm.adsorption

        # transfer unit of n_adsorbed from mol per pore to mol per kg adsorbent
        match self.geometry:
            case 'Spherical':
                n_adsorbed = n_adsorbed_per_pore/(4*np.pi*(self.pore_size)**2) * self.specific_surface_area
            case 'Cylindrical':
                n_adsorbed = n_adsorbed_per_pore/(2*np.pi*self.pore_size*1*ANGSTROM) * self.specific_surface_area
            case 'Cartesian':
                n_adsorbed = n_adsorbed_per_pore/(1*ANGSTROM*ANGSTROM) * self.specific_surface_area

        # transfer n_adsorbed to 1D array
        n_adsorbed = SIArray1([(n_adsorbed/(MOL/KILOGRAM))[0][0]*MOL/KILOGRAM, (n_adsorbed/(MOL/KILOGRAM))[1][0]*MOL/KILOGRAM])
        return n_adsorbed
    

    def get_enthalpy_of_adsorption(self, dft_isotherm, fluid_mixture):
        """
        Get the partial molar enthalpy of adsorption for a fluid mixture from a dft_isotherm object created at a certain temperature T and pressure p.
        """

        #dft_isotherm, pure = self.calc_dft_isotherm_object(T, p, fluid_mixture, calc_enthalpy = True)

        if np.max(fluid_mixture.mole_fractions) >= 1.0:
            pure = True
        else:
            pure = False


        if not pure:

            enthalpy_of_adsorption_SI = dft_isotherm.partial_molar_enthalpy_of_adsorption

            ## adjust SI units so that it is similar to pure component case
            #enthalpy_of_adsorption_unitless = enthalpy_of_adsorption_SI / (JOULE/MOL)
            #enthalpy_of_adsorption = [None]*len(fluid_mixture.mole_fractions)
            #for j in range(len(fluid_mixture.mole_fractions)):
            #    enthalpy_of_adsorption[j] = SIArray1([enthalpy_of_adsorption_unitless[j]*JOULE/MOL])

            # transfer enthalpy_of_adsorption to 1D array
            enthalpy_of_adsorption = SIArray1([(enthalpy_of_adsorption_SI/(JOULE/MOL))[0][0]*JOULE/MOL, (enthalpy_of_adsorption_SI/(JOULE/MOL))[1][0]*JOULE/MOL])
        
        else:
            # if mole fraction of one component is 1, the isotherm object is calculated for the pure component to avoid bug in feos
            j_purecomponent = np.argmax(fluid_mixture.mole_fractions)
            
            #enthalpy_of_adsorption = [None]*len(fluid_mixture.mole_fractions)
            #for j in range(len(fluid_mixture.mole_fractions)):
            #    if j == j_purecomponent:
            #        enthalpy_of_adsorption[j] = dft_isotherm.enthalpy_of_adsorption
            #    else:
            #        enthalpy_of_adsorption[j] = SIArray1([0.0*JOULE/MOL])

            enthalpy_of_adsorption = [None]*len(fluid_mixture.mole_fractions)
            for j in range(len(fluid_mixture.mole_fractions)):
                if j == j_purecomponent:
                    enthalpy_of_adsorption[j] = (dft_isotherm.enthalpy_of_adsorption/(JOULE/MOL))[0]*JOULE/MOL
                else:
                    enthalpy_of_adsorption[j] = 0.0*JOULE/MOL

            enthalpy_of_adsorption = SIArray1(enthalpy_of_adsorption)

        return enthalpy_of_adsorption
    

    def adsorbed_amount(self, T, p, fluid_mixture):
        """
        Calculate the uptake of a fluid mixture at a certain temperature T and pressure p.
        """
        dft_isotherm, _ = self.calc_dft_isotherm_object(T, p, fluid_mixture, calc_enthalpy = False)
        n_adsorbed = self.get_adsorbed_amount(dft_isotherm)
        return n_adsorbed
    
    def enthalpy_of_adsorption(self, T, p, fluid_mixture):
        """
        Calculate the partial molar enthalpy of adsorption for a fluid mixture at a certain temperature T and pressure p.
        """
        dft_isotherm, _ = self.calc_dft_isotherm_object(T, p, fluid_mixture, calc_enthalpy = True)
        enthalpy_of_adsorption = self.get_enthalpy_of_adsorption(dft_isotherm, fluid_mixture)
        return enthalpy_of_adsorption
    
    def adsorbed_amount_and_enthalpy_of_adsorption(self, T, p, fluid_mixture):
        """
        Calculate the uptake of a fluid mixture and the partial molar enthalpy of adsorption at a certain temperature T and pressure p.
        """
        dft_isotherm, _ = self.calc_dft_isotherm_object(T, p, fluid_mixture, calc_enthalpy = True)
        n_adsorbed = self.get_adsorbed_amount(dft_isotherm)
        enthalpy_of_adsorption = self.get_enthalpy_of_adsorption(dft_isotherm, fluid_mixture)
        return n_adsorbed, enthalpy_of_adsorption