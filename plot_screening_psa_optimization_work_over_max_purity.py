# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

import pandas as pd
import matplotlib.pyplot as plt

path = 'results/screening_opt/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70_pdes_opt_20240529_163202/collected_results_sorted.csv'

fontsize = 11
figsize = (3.5, 3.0)

# Read the CSV file
df = pd.read_csv(path)

# set fonts for plots to latex fonts
plt.rcParams.update({"text.usetex": True, "pgf.texsystem": "pdflatex", 'font.family': 'sans-serif', 'font.sans-serif': 'Computer Modern Sans Serif', 'font.size': fontsize})

# create figure
fig, ax = plt.subplots(figsize=figsize)


# Create a scatter plot
scatter = ax.scatter(df['max_purity']*100, df['specific_work_in_kJ/kg'], c=df['p_des_in_Pa']/1000, cmap='viridis', s=20)

ax.set_xlabel('maximum possible purity in \%')
ax.set_ylabel('specific work in kJ/kg')

# Add colorbar using the mappable object
cbar = plt.colorbar(scatter)
cbar.set_label('desorption pressure in kPa')

# Set y-axis limits
#ax.set_ylim(0, 650)
ax.set_ylim(0, 1150)
ax.set_xlim(90, 100)

#add more ticks to the x-axis
#ax.set_xticks([0, 1, 2, 3, 4, 5])
ax.set_xticks([90, 92, 94, 96, 98, 100])

# add more ticks to the colorbar
#cbar.set_ticks([91, 92, 93, 94, 95, 96, 97, 98])

fig.savefig(f'{path[:-4]}_scatter_work_over_maxpurity.pdf', bbox_inches='tight')
