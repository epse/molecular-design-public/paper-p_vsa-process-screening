# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering
"""
Process evaluation of a PSA process model.
Based on and adapted from Maring & Webley 2013
Process model is solved with trf (Trust Region Reflective algorithm) of scipy.optimize.least_squares
author: Fabian Mayer, Energy & Process Systems Engineering, ETH Zurich
date: 2023-12-20
"""
from feos.si import *
from feos.eos import State, Contributions

import numpy as np
import pandas as pd
from scipy.optimize import least_squares
import copy


def psa_process(process_conditions, adsorbent, feed_mixture, full_cycle=True, enthalpy_mean=False):
    """
    Function to evaluate the PSA process model.

    Parameters
    ----------
    process_conditions: ProcessConditions object
        Object containing the process conditions.
    adsorbent: Adsorbent object
        Object containing the adsorbent properties.
    feed_mixture: FeedMixture object
        Object containing the feed mixture properties.
    full_cycle: bool
        If True, the full cycle is evaluated, if False, only the blowdown is evaluated.
    enthalpy_mean: bool
        If True, the enthalpy of adsorption is estimated as mean of enthalpies at T_i, p_i and T_i-1, p_i-1. If False, the enthalpy of adsorption is calculated at T_i_mean and p_i_mean.

    Returns
    -------
    df_process_results: pandas dataframe
        Dataframe containing the final results of the process evaluation including process objectives.
    df_blowdown: pandas dataframe
        Dataframe containing the results of the blowdown steps.
    df_repressurization: pandas dataframe
        Dataframe containing the results of the repressurization steps.
    """
        
    
    #%% set bounds for variables of the process model
    bounds = {'blowdown': {'molefraction_heavy': [0.0, 1.0], 'temperature': [250.0, 500.0], 'delta_N': [-10.0, 0.0]},
              'repressurization': {'molefraction_heavy': [0.0, 1.0], 'temperature': [250.0, 500.0], 'delta_N': [0.0, 10.0]}}

    
    #%% define balance equations of the process model

    def balances_step(x):
        """
        Equations for one step of the blowdown or repressurization.
        Component balances for heavy and light product and energy balance are set up and the equations should result in 0 for x.
        x = [molefraction_heavy, T, delta_N]
        """
        
        #print(x)
        # unpack variables

        if np.isnan(x[0]):
            molefraction_heavy_i = molefraction_heavy[i-1]
        else:
            if process_step == "blowdown":
                molefraction_heavy_i = x[0]
            elif process_step == "repressurization":
                molefraction_heavy_i = x[0]
            else:
                raise ValueError("process_step must be 'blowdown' or 'repressurization'")
        if np.isnan(x[1]):
            T_i = T[i-1]
        else:
            if process_step == "blowdown":
                T_i = x[1]*KELVIN
            elif process_step == "repressurization":
                T_i = x[1]*KELVIN
            else:
                raise ValueError("process_step must be 'blowdown' or 'repressurization'")
        if np.isnan(x[2]):
            delta_N_i = delta_N[i-1]
        else:
            if process_step == "blowdown":
                delta_N_i = x[2]*MOL
            elif process_step == "repressurization":
                delta_N_i = x[2]*MOL
            else:
                raise ValueError("process_step must be 'blowdown' or 'repressurization'")
            
        
        molefraction_light_i = np.min([np.max([1 - molefraction_heavy_i, 0.0]), 1.0])

        molefraction_heavy_i_mean = (molefraction_heavy_i + molefraction_heavy[i-1])/2
        molefraction_light_i_mean = 1.0 - molefraction_heavy_i_mean

        if process_step == "blowdown":
            molefraction_delta_N_heavy = molefraction_heavy_i_mean
            molefraction_delta_N_light = molefraction_light_i_mean
        elif process_step == "repressurization":
            molefraction_delta_N_heavy = feed_mixture.mole_fractions[0]
            molefraction_delta_N_light = feed_mixture.mole_fractions[1]
        else:
            raise ValueError("process_step must be 'blowdown' or 'repressurization'")

        T_i_mean = (T_i + T[i-1])/2
        p_i_mean = (p[i] + p[i-1])/2

        # adjust molefraction of mixture in gas phase inside the adsorber
        gas_phase = copy.copy(feed_mixture)
        gas_phase_mean = copy.copy(feed_mixture)
        gas_phase.update_mole_fractions([molefraction_heavy_i, molefraction_light_i])
        gas_phase_mean.update_mole_fractions([molefraction_heavy_i_mean, molefraction_light_i_mean])

        # calculate adsorbed amount and enthalpy of adsorption
        if enthalpy_mean == True:
            n_adsorbed_i, enthalpy_adsorption_iend = adsorbent.adsorbed_amount_and_enthalpy_of_adsorption(T_i, p[i], gas_phase)
            enthalpy_adsorption_heavy[i] = enthalpy_adsorption_iend[0]
            enthalpy_adsorption_light[i] = enthalpy_adsorption_iend[1]
            enthalpy_adsorption_heavy_i = (enthalpy_adsorption_heavy[i] + enthalpy_adsorption_heavy[i-1])/2
            enthalpy_adsorption_light_i = (enthalpy_adsorption_light[i] + enthalpy_adsorption_light[i-1])/2
        else:
            n_adsorbed_i = adsorbent.adsorbed_amount(T_i, p[i], gas_phase)
            enthalpy_adsorption_i = adsorbent.enthalpy_of_adsorption(T_i_mean, p_i_mean, gas_phase_mean)
            enthalpy_adsorption_heavy_i = enthalpy_adsorption_i[0]
            enthalpy_adsorption_light_i = enthalpy_adsorption_i[1]
            enthalpy_adsorption_heavy[i] = enthalpy_adsorption_i[0]
            enthalpy_adsorption_light[i] = enthalpy_adsorption_i[1]
        

        # transfer n_adsorbed to N_adsorbed_heavy and N_adsorbed_light
        #N_adsorbed_heavy_i = (n_adsorbed_i/(MOL/KILOGRAM))[0][0]*(MOL/KILOGRAM) * adsorbent.mass
        #N_adsorbed_light_i = (n_adsorbed_i/(MOL/KILOGRAM))[1][0]*(MOL/KILOGRAM) * adsorbent.mass
        N_adsorbed_heavy_i = n_adsorbed_i[0] * adsorbent.mass
        N_adsorbed_light_i = n_adsorbed_i[1] * adsorbent.mass


        # calculate amounts in gas phase
        gas_phase_state = State(feed_mixture.eos, T_i, pressure=p[i], volume=adsorbent.void_volume, molefracs=np.array(gas_phase.mole_fractions))
        N_gas_phase_heavy_i = gas_phase_state.moles[0]
        N_gas_phase_light_i = gas_phase_state.moles[1]

        # get enthalpy of adsorption for heavy and light component
        #enthalpy_adsorption_heavy_i = (enthalpy_adsorption_i/(JOULE/MOL))[0][0]*(JOULE/MOL)
        #enthalpy_adsorption_heavy_i = enthalpy_adsorption_i[0][0]
        #enthalpy_adsorption_heavy_i = enthalpy_adsorption_i[0]
        #enthalpy_adsorption_light_i = (enthalpy_adsorption_i/(JOULE/MOL))[1][0]*(JOULE/MOL)
        #enthalpy_adsorption_light_i = enthalpy_adsorption_i[1][0]
        #enthalpy_adsorption_light_i = enthalpy_adsorption_i[1]


        # calculate component balances
        component_balance_heavy = N_adsorbed_heavy_i - N_adsorbed_heavy[i-1] + N_gas_phase_heavy_i - N_gas_phase_heavy[i-1] - delta_N_i*molefraction_delta_N_heavy
        component_balance_light = N_adsorbed_light_i - N_adsorbed_light[i-1] + N_gas_phase_light_i - N_gas_phase_light[i-1] - delta_N_i*molefraction_delta_N_light
        

        # calculate energy balance
        energy_balance = - adsorbent.mass*adsorbent.cp*(T_i-T[i-1]) + (N_adsorbed_heavy_i - N_adsorbed_heavy[i-1])*enthalpy_adsorption_heavy_i + (N_adsorbed_light_i - N_adsorbed_light[i-1])*enthalpy_adsorption_light_i

        # nan handling
        if np.any(np.isnan([component_balance_heavy/MOL, component_balance_light/MOL, energy_balance/JOULE])):
            return 10000, 10000, 10000

        #print(component_balance_heavy/MOL, component_balance_light/MOL, energy_balance/JOULE)
        return (1000*component_balance_heavy/MOL, 1000*component_balance_light/MOL, energy_balance/(KILO*JOULE))

    


    def balances_step_pure_heavy(x):
        """
        Equations for one step of the blowdown when only the heavy component is left.
        Component balances for heavy product and energy balance are set up and the equations should result in 0 for x.
        x = [T, delta_N]
        """
            

        # unpack variables
        if np.isnan(x[0]):
            T_i = T[i-1]
        else:
            if process_step == "blowdown":
                T_i = x[0]*KELVIN
            elif process_step == "repressurization":
                T_i = x[0]*KELVIN
            else:
                raise ValueError("process_step must be 'blowdown' or 'repressurization'")
        if np.isnan(x[1]):
            delta_N_i = delta_N[i-1]
        else:
            if process_step == "blowdown":
                delta_N_i = x[1]*MOL
            elif process_step == "repressurization":
                delta_N_i = x[1]*MOL
            else:
                raise ValueError("process_step must be 'blowdown' or 'repressurization'")

        
        molefraction_heavy_i = 1.0
        molefraction_heavy_i_mean = (molefraction_heavy_i + molefraction_heavy[i-1])/2
        molefraction_light_i = 1.0 - molefraction_heavy_i
        molefraction_light_i_mean = 1.0 - molefraction_heavy_i_mean

        T_i_mean = (T_i + T[i-1])/2
        p_i_mean = (p[i] + p[i-1])/2

        # adjust molefraction of mixture in gas phase inside the adsorber
        gas_phase = copy.copy(feed_mixture)
        gas_phase_mean = copy.copy(feed_mixture)
        gas_phase.update_mole_fractions([molefraction_heavy_i, molefraction_light_i])
        gas_phase_mean.update_mole_fractions([molefraction_heavy_i_mean, molefraction_light_i_mean])

        # calculate adsorbed amount and enthalpy of adsorption
        if enthalpy_mean == True:
            n_adsorbed_i, enthalpy_adsorption_iend = adsorbent.adsorbed_amount_and_enthalpy_of_adsorption(T_i, p[i], gas_phase)
            enthalpy_adsorption_heavy[i] = enthalpy_adsorption_iend[0]
            enthalpy_adsorption_heavy_i = (enthalpy_adsorption_heavy[i] + enthalpy_adsorption_heavy[i-1])/2
        else:
            n_adsorbed_i = adsorbent.adsorbed_amount(T_i, p[i], gas_phase)
            enthalpy_adsorption_i = adsorbent.enthalpy_of_adsorption(T_i_mean, p_i_mean, gas_phase_mean)
            enthalpy_adsorption_heavy_i = enthalpy_adsorption_i[0]
            enthalpy_adsorption_heavy[i] = enthalpy_adsorption_i[0]
        n_adsorbed_i = adsorbent.adsorbed_amount(T_i, p[i], gas_phase)

        # transfer n_adsorbed to N_adsorbed_heavy
        #N_adsorbed_heavy_i = (n_adsorbed_i/(MOL/KILOGRAM))[0][0]*(MOL/KILOGRAM) * adsorbent.mass
        N_adsorbed_heavy_i = n_adsorbed_i[0] * adsorbent.mass

        # calculate amounts in gas phase
        gas_phase_state = State(feed_mixture.eos, T_i, pressure=p[i], volume=adsorbent.void_volume, molefracs=np.array(gas_phase.mole_fractions))
        N_gas_phase_heavy_i = gas_phase_state.moles[0]

        # calculate enthalpy of adsorption
        #enthalpy_adsorption_i = adsorbent.enthalpy_of_adsorption(T_i_mean, p_i_mean, gas_phase_mean)
        #enthalpy_adsorption_heavy_i = (enthalpy_adsorption_i/(JOULE/MOL))[0][0]*(JOULE/MOL)
        #enthalpy_adsorption_heavy_i = enthalpy_adsorption_i[0][0]
        #enthalpy_adsorption_heavy_i = enthalpy_adsorption_i[0]

        # calculate component balances
        component_balance_heavy = N_adsorbed_heavy_i - N_adsorbed_heavy[i-1] + N_gas_phase_heavy_i - N_gas_phase_heavy[i-1] - delta_N_i*molefraction_heavy_i_mean
        
        # calculate energy balance
        energy_balance = - adsorbent.mass*adsorbent.cp*(T_i-T[i-1]) + (N_adsorbed_heavy_i - N_adsorbed_heavy[i-1])*enthalpy_adsorption_heavy_i

        # nan handling
        if np.any(np.isnan([component_balance_heavy/MOL, energy_balance/JOULE])):
            return 10000, 10000

        return (1000*component_balance_heavy/MOL, energy_balance/(KILO*JOULE))



    
    
    #%% prepare empty lists for results of blowdown steps
    p = process_conditions.p_steps_bd
    T = SIArray1(np.zeros(process_conditions.steps+1)*KELVIN)
    molefraction_heavy = np.zeros(process_conditions.steps+1)
    molefraction_light = np.zeros(process_conditions.steps+1)
    delta_N = SIArray1(np.zeros(process_conditions.steps+1)*MOL)
    N_adsorbed_heavy = SIArray1(np.zeros(process_conditions.steps+1)*MOL)
    N_adsorbed_light = SIArray1(np.zeros(process_conditions.steps+1)*MOL)
    N_gas_phase_heavy = SIArray1(np.zeros(process_conditions.steps+1)*MOL)
    N_gas_phase_light = SIArray1(np.zeros(process_conditions.steps+1)*MOL)
    work = SIArray1(np.zeros(process_conditions.steps+1)*JOULE)
    balance_heavy_error =  np.zeros(process_conditions.steps+1)
    balance_light_error =  np.zeros(process_conditions.steps+1)
    balanance_energy_error =  np.zeros(process_conditions.steps+1)
    enthalpy_adsorption_heavy = SIArray1(np.zeros(process_conditions.steps+1)*JOULE/MOL)
    enthalpy_adsorption_light = SIArray1(np.zeros(process_conditions.steps+1)*JOULE/MOL)

    # set initial values at beginning of blowdown
    process_step = "blowdown"
    T[0] = process_conditions.temperature_feed
    molefraction_heavy[0] = feed_mixture.mole_fractions[0]
    molefraction_light[0] = feed_mixture.mole_fractions[1]
    delta_N[0] = 0*MOL

    # calculate adsorbed amount at beginning of blowdown
    n_adsorbed_0 = adsorbent.adsorbed_amount(T[0], p[0], feed_mixture)

    # transfer n_adsorbed to N_adsorbed_heavy and N_adsorbed_light
    #N_adsorbed_heavy[0] = (n_adsorbed_0/(MOL/KILOGRAM))[0][0]*(MOL/KILOGRAM) * adsorbent.mass
    #N_adsorbed_light[0] = (n_adsorbed_0/(MOL/KILOGRAM))[1][0]*(MOL/KILOGRAM) * adsorbent.mass
    N_adsorbed_heavy[0] = n_adsorbed_0[0] * adsorbent.mass
    N_adsorbed_light[0] = n_adsorbed_0[1] * adsorbent.mass

    # calculate amounts in gas phase at beginning of blowdown
    gas_phase_0_state = State(feed_mixture.eos, T[0], pressure=p[0], volume=adsorbent.void_volume, molefracs=np.array(feed_mixture.mole_fractions))
    N_gas_phase_heavy[0] = gas_phase_0_state.moles[0]
    N_gas_phase_light[0] = gas_phase_0_state.moles[1]

    
    
    #%% solve blowdown steps

    for i in range(1, process_conditions.steps + 1):
        print(f"Step {i} of {process_conditions.steps} in blowdown of adsorbent {adsorbent.name}")

        # set initial guess for x
        x_guess = np.zeros(3)
        if i == 1:
            x_guess[0] = feed_mixture.mole_fractions[0]
            x_guess[1] = process_conditions.temperature_feed/KELVIN
            x_guess[2] = -0.5/process_conditions.steps
        else:
            # avoid that x_guess is nan
            if np.isnan(molefraction_heavy[i-1]):
                x_guess[0] = molefraction_heavy[i-2]
            else:
                x_guess[0] = np.min([molefraction_heavy[i-1] + (molefraction_heavy[i-1] - molefraction_heavy[i-2]), 1.0])
            if np.isnan(T[i-1]/KELVIN):
                x_guess[1] = T[i-2]/KELVIN
            else:
                x_guess[1] = (T[i-1] + (T[i-1] - T[i-2]))/KELVIN
            if np.isnan(delta_N[i-1]/MOL):
                x_guess[2] = delta_N[i-2]/MOL
            else:
                x_guess[2] = (delta_N[i-1] + (delta_N[i-1] - delta_N[i-2]))/MOL
        
    

        if molefraction_heavy[i-1] < 1.0-1e-4:
            bounds_bd = ([molefraction_heavy[i-1], bounds[process_step]['temperature'][0], bounds[process_step]['delta_N'][0]], [bounds[process_step]['molefraction_heavy'][1], T[i-1]/KELVIN, bounds[process_step]['delta_N'][1]])
            result_least_squares = least_squares(balances_step, x_guess, max_nfev=1000, bounds=bounds_bd)
            x_blowdown = result_least_squares.x

            balance_heavy_error[i] = result_least_squares.fun[0]
            balance_light_error[i] = result_least_squares.fun[1]
            balanance_energy_error[i] = result_least_squares.fun[2]
        else:
            print("Only heavy component left in blowdown step.")
            x_guess_pure_heavy = np.zeros(2)
            x_guess_pure_heavy[0] = x_guess[1]
            x_guess_pure_heavy[1] = x_guess[2]
            bounds_bd = ([bounds[process_step]['temperature'][0], bounds[process_step]['delta_N'][0]], [T[i-1]/KELVIN, bounds[process_step]['delta_N'][1]])
            result_least_squares = least_squares(balances_step_pure_heavy, x_guess_pure_heavy, max_nfev=1000, bounds=bounds_bd)
            x_blowdown_temp = result_least_squares.x

            balance_heavy_error[i] = result_least_squares.fun[0]
            balanance_energy_error[i] = result_least_squares.fun[1]
            x_blowdown = np.zeros(3)
            x_blowdown[0] = 1.0
            x_blowdown[1] = x_blowdown_temp[0]
            x_blowdown[2] = x_blowdown_temp[1]
        
        

        molefraction_heavy[i] = np.min([x_blowdown[0], 1.0])
        T[i] = x_blowdown[1]*KELVIN
        delta_N[i] = x_blowdown[2]*MOL
        molefraction_light[i] = np.max([1 - molefraction_heavy[i], 0.0])


        # adjust molefraction of mixture in gas phase inside the adsorber
        gas_phase_i = copy.copy(feed_mixture)
        gas_phase_i.update_mole_fractions([molefraction_heavy[i], molefraction_light[i]])

        # calculate adsorbed amount
        n_adsorbed_i = adsorbent.adsorbed_amount(T[i], p[i], gas_phase_i)

        # transfer n_adsorbed to N_adsorbed_heavy and N_adsorbed_light
        #N_adsorbed_heavy[i] = (n_adsorbed_i/(MOL/KILOGRAM))[0][0]*(MOL/KILOGRAM) * adsorbent.mass
        #N_adsorbed_light[i] = (n_adsorbed_i/(MOL/KILOGRAM))[1][0]*(MOL/KILOGRAM) * adsorbent.mass
        N_adsorbed_heavy[i] = n_adsorbed_i[0] * adsorbent.mass
        N_adsorbed_light[i] = n_adsorbed_i[1] * adsorbent.mass
    
        # calculate amounts in gas phase
        gas_phase_i_state = State(feed_mixture.eos, T[i], pressure=p[i], volume=adsorbent.void_volume, molefracs=np.array(gas_phase_i.mole_fractions))
        N_gas_phase_heavy[i] = gas_phase_i_state.moles[0]
        N_gas_phase_light[i] = gas_phase_i_state.moles[1]

        #a=gas_phase_i_state.molar_isochoric_heat_capacity(contributions=Contributions.Total)
        #a=gas_phase_i_state.molar_isochoric_heat_capacity()
        #print(a)

        

        # add work of blowdown step if pressure is below 1 bar
        if p[i] < 1*BAR:
            gas_phase_i.calc_ratio_heat_capacities_mixture()
            k = gas_phase_i.ratio_heat_capacities_mixture
            eta = process_conditions.isentropic_efficiency
            work[i] = k/(k-1) / eta * T[i] * delta_N[i] * RGAS * ((1*BAR/p[i])**((k-1)/k)-1)
        else:
            work[i] = 0*JOULE

    total_work = np.sum(work/JOULE)*JOULE
    delta_N_bd_heavy_total = np.sum(molefraction_heavy*delta_N/MOL)*MOL
    delta_m_bd_heavy_total = delta_N_bd_heavy_total * feed_mixture.pcsaft_parameters.pure_records[0].molarweight*(GRAM/MOL)
    delta_N_bd_light_total = np.sum(molefraction_light*delta_N/MOL)*MOL
    delta_m_bd_light_total = delta_N_bd_light_total * feed_mixture.pcsaft_parameters.pure_records[1].molarweight*(GRAM/MOL)

    specific_work = total_work/delta_m_bd_heavy_total
    purity_m = delta_m_bd_heavy_total/(delta_m_bd_heavy_total + delta_m_bd_light_total)
    purity = delta_N_bd_heavy_total/(delta_N_bd_heavy_total + delta_N_bd_light_total)
    working_capacity = (N_adsorbed_heavy[0] - N_adsorbed_heavy[len(N_adsorbed_heavy)-1])/adsorbent.mass
    recovery_m = -1
    recovery = -1
    
    
    T_bd = T
    p_bd = p
    molefraction_bd_heavy = molefraction_heavy
    molefraction_bd_light = molefraction_light
    delta_N_bd = delta_N
    N_adsorbed_bd_heavy = N_adsorbed_heavy
    N_adsorbed_bd_light = N_adsorbed_light
    N_gas_phase_bd_heavy = N_gas_phase_heavy
    N_gas_phase_bd_light = N_gas_phase_light

    

    # store results of blowdown steps in pandas dataframe
    df_blowdown = pd.DataFrame({
        'p_in_Pa': p_bd/PASCAL,
        'T_in_K': T_bd/KELVIN,
        'molefraction_heavy': molefraction_bd_heavy,
        'molefraction_light': molefraction_bd_light,
        'delta_N_in_mol': delta_N_bd/MOL,
        'N_adsorbed_heavy_in_mol': N_adsorbed_bd_heavy/MOL,
        'N_adsorbed_light_in_mol': N_adsorbed_bd_light/MOL,
        'N_gas_phase_heavy_in_mol': N_gas_phase_bd_heavy/MOL,
        'N_gas_phase_light_in_mol': N_gas_phase_bd_light/MOL,
        'work_in_J': work/JOULE,
        'balance_heavy_error': balance_heavy_error,
        'balance_light_error': balance_light_error,
        'balance_energy_error': balanance_energy_error,
    })

    # store final results of blowdown including specific work and purity in pandas dataframe, add empty entry for recovery
    df_process_results = pd.DataFrame({
        'delta_N_bd_heavy_total_in_mol': [delta_N_bd_heavy_total/MOL],
        'delta_N_bd_light_total_in_mol': [delta_N_bd_light_total/MOL],
        'delta_m_bd_heavy_total_in_g': [delta_m_bd_heavy_total/GRAM],
        'delta_m_bd_light_total_in_g': [delta_m_bd_light_total/GRAM],
        'total_work_in_J': [total_work/JOULE],
        'specific_work_in_kJ/kg': [specific_work/(KILO*JOULE/(KILOGRAM))],
        'purity_m': [purity_m],
        'purity': [purity],
        'working_capacity_in_mol/kg': [working_capacity/(MOL/KILOGRAM)]
    })



    #%% prepare empty lists for results of repressurization steps
    p = process_conditions.p_steps_rp
    T = SIArray1(np.zeros(process_conditions.steps+1)*KELVIN)
    molefraction_heavy = np.zeros(process_conditions.steps+1)
    molefraction_light = np.zeros(process_conditions.steps+1)
    delta_N = SIArray1(np.zeros(process_conditions.steps+1)*MOL)
    N_adsorbed_heavy = SIArray1(np.zeros(process_conditions.steps+1)*MOL)
    N_adsorbed_light = SIArray1(np.zeros(process_conditions.steps+1)*MOL)
    N_gas_phase_heavy = SIArray1(np.zeros(process_conditions.steps+1)*MOL)
    N_gas_phase_light = SIArray1(np.zeros(process_conditions.steps+1)*MOL)
    balance_heavy_error =  np.zeros(process_conditions.steps+1)
    balance_light_error =  np.zeros(process_conditions.steps+1)
    balanance_energy_error =  np.zeros(process_conditions.steps+1)


    # store empty lists in pandas dataframe
    df_repressurization = pd.DataFrame({
        'p_in_Pa': p/PASCAL,
        'T_in_K': T,
        'molefraction_heavy': molefraction_heavy,
        'molefraction_light': molefraction_light,
        'delta_N_in_mol': delta_N/MOL,
        'N_adsorbed_heavy_in_mol': N_adsorbed_heavy/MOL,
        'N_adsorbed_light_in_mol': N_adsorbed_light/MOL,
        'N_gas_phase_heavy_in_mol': N_gas_phase_heavy/MOL,
        'N_gas_phase_light_in_mol': N_gas_phase_light/MOL,
        'balance_heavy_error': balance_heavy_error,
        'balance_light_error': balance_light_error,
        'balance_energy_error': balanance_energy_error,
    })


    #%% solve repressurization steps if full cycle should be evaluated

    if full_cycle == True:
        

        # set initial values at beginning of repressurization
        process_step = "repressurization"
        T[0] = (T_bd/KELVIN)[-1]*KELVIN
        molefraction_heavy[0] = molefraction_bd_heavy[-1]
        molefraction_light[0] = molefraction_bd_light[-1]
        delta_N[0] = 0*MOL

        gas_phase_rp_0 = copy.copy(feed_mixture)
        gas_phase_rp_0.update_mole_fractions([molefraction_heavy[0], molefraction_light[0]])

        # calculate adsorbed amount at beginning of repressurization
        n_adsorbed_rp_0 = adsorbent.adsorbed_amount(T[0], p[0], gas_phase_rp_0)

        # transfer n_adsorbed to N_adsorbed_heavy and N_adsorbed_light
        #N_adsorbed_heavy[0] = (n_adsorbed_rp_0/(MOL/KILOGRAM))[0][0]*(MOL/KILOGRAM) * adsorbent.mass
        #N_adsorbed_light[0] = (n_adsorbed_rp_0/(MOL/KILOGRAM))[1][0]*(MOL/KILOGRAM) * adsorbent.mass
        N_adsorbed_heavy[0] = n_adsorbed_rp_0[0] * adsorbent.mass
        N_adsorbed_light[0] = n_adsorbed_rp_0[1] * adsorbent.mass

        # calculate amounts in gas phase at beginning of repressurization
        gas_phase_rp_0_state = State(feed_mixture.eos, T[0], pressure=p[0], volume=adsorbent.void_volume, molefracs=np.array(gas_phase_rp_0.mole_fractions))
        N_gas_phase_heavy[0] = gas_phase_rp_0_state.moles[0]
        N_gas_phase_light[0] = gas_phase_rp_0_state.moles[1]


        #%% solve repressurization steps

        for i in range(1, process_conditions.steps + 1):
            print(f"Step {i} of {process_conditions.steps} in repressurization of adsorbent {adsorbent.name}")

            # set initial guess for x
            x_guess = np.zeros(3)
            if i == 1 or molefraction_heavy[i-1] == 1.0:
                x_guess[0] = np.max([np.min([molefraction_heavy[0]-(molefraction_heavy[0]-feed_mixture.mole_fractions[0])/(process_conditions.steps), 1.0]), 0.0])
                x_guess[1] = T[0]/KELVIN
                x_guess[2] = -np.mean(delta_N_bd/MOL)
            else:
                # avoid that x_guess is nan
                if np.isnan(molefraction_heavy[i-1]):
                    x_guess[0] = molefraction_heavy[i-2]
                else:
                    x_guess[0] = np.max([np.min([molefraction_heavy[i-1] + (molefraction_heavy[i-1] - molefraction_heavy[i-2]), 1.0]), 0.0])
                if np.isnan(T[i-1]/KELVIN):
                    x_guess[1] = T[i-2]/KELVIN
                else:
                    x_guess[1] = (T[i-1] + (T[i-1] - T[i-2]))/KELVIN
                if np.isnan(delta_N[i-1]/MOL):
                    x_guess[2] = delta_N[i-2]/MOL
                else:
                    x_guess[2] = np.max([(delta_N[i-1] + (delta_N[i-1] - delta_N[i-2]))/MOL, 0.0])


            bounds_rp = ([bounds[process_step]['molefraction_heavy'][0], T[i-1]/KELVIN, bounds[process_step]['delta_N'][0]], [molefraction_heavy[i-1], bounds[process_step]['temperature'][1], bounds[process_step]['delta_N'][1]])
            result_least_squares = least_squares(balances_step, x_guess, max_nfev=1000, bounds=bounds_rp)
            x_repressurization = result_least_squares.x
            
            balance_heavy_error[i] = result_least_squares.fun[0]
            balance_light_error[i] = result_least_squares.fun[1]
            balanance_energy_error[i] = result_least_squares.fun[2]


            molefraction_heavy[i] = x_repressurization[0]
            T[i] = x_repressurization[1]*KELVIN
            delta_N[i] = x_repressurization[2]*MOL
            molefraction_light[i] = 1.0 - molefraction_heavy[i]


            # adjust molefraction of mixture in gas phase inside the adsorber
            gas_phase_i = copy.copy(feed_mixture)
            gas_phase_i.update_mole_fractions([molefraction_heavy[i], molefraction_light[i]])

            # calculate adsorbed amount
            n_adsorbed_i = adsorbent.adsorbed_amount(T[i], p[i], gas_phase_i)

            # transfer n_adsorbed to N_adsorbed_heavy and N_adsorbed_light
            #N_adsorbed_heavy[i] = (n_adsorbed_i/(MOL/KILOGRAM))[0][0]*(MOL/KILOGRAM) * adsorbent.mass
            #N_adsorbed_light[i] = (n_adsorbed_i/(MOL/KILOGRAM))[1][0]*(MOL/KILOGRAM) * adsorbent.mass
            N_adsorbed_heavy[i] = n_adsorbed_i[0] * adsorbent.mass
            N_adsorbed_light[i] = n_adsorbed_i[1] * adsorbent.mass

            # calculate amounts in gas phase
            gas_phase_i_state = State(feed_mixture.eos, T[i], pressure=p[i], volume=adsorbent.void_volume, molefracs=np.array(gas_phase_i.mole_fractions))
            N_gas_phase_heavy[i] = gas_phase_i_state.moles[0]
            N_gas_phase_light[i] = gas_phase_i_state.moles[1]

        
        delta_N_rp_heavy_total = np.sum(feed_mixture.mole_fractions[0]*delta_N/MOL)*MOL
        delta_m_rp_heavy_total = delta_N_rp_heavy_total * feed_mixture.pcsaft_parameters.pure_records[0].molarweight*(GRAM/MOL)
        delta_N_rp_light_total = np.sum(feed_mixture.mole_fractions[1]*delta_N/MOL)*MOL
        delta_m_rp_light_total = delta_N_rp_light_total * feed_mixture.pcsaft_parameters.pure_records[1].molarweight*(GRAM/MOL)


        T_rp = T
        p_rp = p
        molefraction_rp_heavy = molefraction_heavy
        molefraction_rp_light = molefraction_light
        delta_N_rp = delta_N
        N_adsorbed_rp_heavy = N_adsorbed_heavy
        N_adsorbed_rp_light = N_adsorbed_light
        N_gas_phase_rp_heavy = N_gas_phase_heavy
        N_gas_phase_rp_light = N_gas_phase_light

        # store results of repressurization in pandas dataframe
        df_repressurization = pd.DataFrame({
            'p_in_Pa': p_rp/PASCAL,
            'T_in_K': T_rp/KELVIN,
            'molefraction_heavy': molefraction_rp_heavy,
            'molefraction_light': molefraction_rp_light,
            'delta_N_in_mol': delta_N_rp/MOL,
            'N_adsorbed_heavy_in_mol': N_adsorbed_rp_heavy/MOL,
            'N_adsorbed_light_in_mol': N_adsorbed_rp_light/MOL,
            'N_gas_phase_heavy_in_mol': N_gas_phase_rp_heavy/MOL,
            'N_gas_phase_light_in_mol': N_gas_phase_rp_light/MOL,
            'balance_heavy_error': balance_heavy_error,
            'balance_light_error': balance_light_error,
            'balance_energy_error': balanance_energy_error,
        })

        #%% solve feed step

        delta_N_feed = (molefraction_rp_light[-1]*(N_gas_phase_bd_heavy[0] + N_adsorbed_bd_heavy[0]) - molefraction_rp_heavy[-1]*(N_gas_phase_bd_light[0] + N_adsorbed_bd_light[0]) - molefraction_rp_light[-1]*((N_gas_phase_rp_heavy/MOL)[-1]*MOL + (N_adsorbed_rp_heavy/MOL)[-1]*MOL) + molefraction_rp_heavy[-1]*((N_gas_phase_rp_light/MOL)[-1]*MOL + (N_adsorbed_rp_light/MOL)[-1]*MOL)) / (feed_mixture.mole_fractions[0]*molefraction_rp_light[-1] - feed_mixture.mole_fractions[1]*molefraction_rp_heavy[-1])

        delta_N_waste = ((N_gas_phase_bd_heavy[0] + N_adsorbed_bd_heavy[0]) - ((N_gas_phase_rp_heavy/MOL)[-1]*MOL + (N_adsorbed_rp_heavy/MOL)[-1]*MOL) - feed_mixture.mole_fractions[0]*delta_N_feed) / molefraction_rp_heavy[-1]
        delta_N_waste_check = ((N_gas_phase_bd_light[0] + N_adsorbed_bd_light[0]) - ((N_gas_phase_rp_light/MOL)[-1]*MOL + (N_adsorbed_rp_light/MOL)[-1]*MOL) - feed_mixture.mole_fractions[1]*delta_N_feed) / molefraction_rp_light[-1]

        if abs(delta_N_waste/MOL - delta_N_waste_check/MOL) > 1e-6:
            print("delta_N_waste and delta_N_waste_check do not match.")

        delta_N_feed_heavy = feed_mixture.mole_fractions[0]*delta_N_feed
        delta_m_feed_heavy = delta_N_feed_heavy * feed_mixture.pcsaft_parameters.pure_records[0].molarweight*(GRAM/MOL)
        delta_N_feed_light = feed_mixture.mole_fractions[1]*delta_N_feed
        delta_m_feed_light = delta_N_feed_light * feed_mixture.pcsaft_parameters.pure_records[1].molarweight*(GRAM/MOL)

        recovery_m = -delta_m_bd_heavy_total/(delta_m_rp_heavy_total + delta_m_feed_heavy)
        recovery = -delta_N_bd_heavy_total/(delta_N_rp_heavy_total + delta_N_feed_heavy)
        
        #%% store final results of repressurization including recovery in pandas dataframe
        df_process_results['delta_N_rp_heavy_total_in_mol'] = delta_N_rp_heavy_total/MOL
        df_process_results['delta_N_rp_light_total_in_mol'] = delta_N_rp_light_total/MOL
        df_process_results['delta_m_rp_heavy_total_in_g'] = delta_m_rp_heavy_total/GRAM
        df_process_results['delta_m_rp_light_total_in_g'] = delta_m_rp_light_total/GRAM
        df_process_results['delta_N_feed_heavy_in_mol'] = delta_N_feed_heavy/MOL
        df_process_results['delta_m_feed_heavy_in_g'] = delta_m_feed_heavy/GRAM
        df_process_results['delta_N_feed_light_in_mol'] = delta_N_feed_light/MOL
        df_process_results['delta_m_feed_light_in_g'] = delta_m_feed_light/GRAM
        df_process_results['delta_N_waste_in_mol'] = delta_N_waste/MOL
        df_process_results['recovery_m'] = recovery_m
        df_process_results['recovery'] = recovery




    return df_process_results, df_blowdown, df_repressurization
# %%
