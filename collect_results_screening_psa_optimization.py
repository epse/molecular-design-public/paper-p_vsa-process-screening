# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering
# code to include results of process optimization-based screening into one csv-file

import os
import pandas as pd

# Directory containing the folders
directory = 'results/screening_opt/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70_pdes_opt_20240529_163202'

# csv-file with information on maximum possible purity
max_purity_file = 'prisma-isotherm-simulated_rsm0011-4602_DFT128fit_max_purity_20240529_161321_sorted_top41'

# List to store the extracted information
data = []

# read the csv-file with information on maximum possible purity
df_max_purity = pd.read_csv(f'{directory}/{max_purity_file}.csv')

# Loop over folders in the directory
for folder in os.listdir(directory):
    folder_path = os.path.join(directory, folder)
    
    # Check if the item in the directory is a folder
    if os.path.isdir(folder_path):
        csv_file = None

        # Loop over files in the folder
        for file in os.listdir(folder_path):
            file_path = os.path.join(folder_path, file)
            
            # Check if the file is a CSV file and starts with a number
            if os.path.isfile(file_path) and file.startswith(tuple('0123456789')):
                csv_file = file_path
                break

        if csv_file is not None:
            # Check if the CSV file exists in the folder
            if os.path.isfile(csv_file):
                # Read the CSV file using pandas
                df = pd.read_csv(csv_file)
                
                # Extract specific information from the CSV file
                rows = df.loc[df['Unnamed: 0'].notnull(), 'Unnamed: 0'].values
                values = df.loc[df['value'].notnull(), 'value'].values

                # find number of row with specific work and purity
                for i in range(len(rows)):
                    if rows[i] == 'specific_work_in_kJ/kg':
                        row_work = i
                    elif rows[i] == 'purity':
                        row_purity = i
                    elif rows[i] == 'p_des_in_Pa':
                        row_p_des = i
                    #elif rows[i] == 'recovery':
                    #    row_recovery = i
                

                specific_work = values[row_work]
                purity = values[row_purity]
                p_des = values[row_p_des]
                #recovery = values[row_recovery]
                
                if file[1]=='_':
                    no = file[0]
                    name = file[2:16]
                    rsm = file[2:9]
                    ccdc = file[10:16]
                else:
                    no = file[:2]
                    name = file[3:17]
                    rsm = file[3:10]
                    ccdc = file[11:17]

                # find maximum possible purity
                max_purity = df_max_purity.loc[df_max_purity['MOF'] == rsm, 'max_purity'].values[0]                

                # Store the extracted information in the data list
                data.append({'no': no, 'name': name, 'rsm': rsm, 'ccdc': ccdc, 'p_des_in_Pa': p_des, 'specific_work_in_kJ/kg': specific_work, 'purity': purity, 'max_purity': max_purity})
        

# Path to the new CSV file
output_file = f'{directory}/collected_results.csv'

# Convert the data list to a pandas DataFrame
df_output = pd.DataFrame(data)

# Write the extracted information to the new CSV file
df_output.to_csv(output_file, index=False)
