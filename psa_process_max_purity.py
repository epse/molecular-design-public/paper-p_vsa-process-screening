# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering
"""
Function to calculate the theoretical maximum possible purity of the PSA process model by Maring et al. 2013.
author: Fabian Mayer, Energy & Process Systems Engineering, ETH Zurich
date: 2023-11-23
"""

from feos.si import *
from feos.eos import State

import numpy as np

def calc_max_purity(process_conditions, adsorbent, feed_mixture):

    # calculate adsorbed amount at beginning of blowdown
    n_adsorbed_0 = adsorbent.adsorbed_amount(process_conditions.temperature_feed, process_conditions.pressure_feed, feed_mixture)

    # transfer n_adsorbed to N_adsorbed_heavy and N_adsorbed_light
    N_adsorbed_heavy = n_adsorbed_0[0] * adsorbent.mass
    N_adsorbed_light = n_adsorbed_0[1] * adsorbent.mass

    # calculate amounts in gas phase at beginning of blowdown
    gas_phase_0_state = State(feed_mixture.eos, process_conditions.temperature_feed, pressure=process_conditions.pressure_feed, volume=adsorbent.void_volume, molefracs=np.array(feed_mixture.mole_fractions))
    N_gas_phase_heavy = gas_phase_0_state.moles[0]
    N_gas_phase_light = gas_phase_0_state.moles[1]


    max_purity = (N_adsorbed_heavy + N_gas_phase_heavy)/(N_adsorbed_heavy + N_gas_phase_heavy + N_adsorbed_light + N_gas_phase_light)

    return max_purity