# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering
"""
executable code for evaluating the PSA process model by Maring et al. 2013 for a csv-file with 1D-DFT parameters of adsorbents for varying variables, e.g., desorption pressure
author: Fabian Mayer, Energy & Process Systems Engineering, ETH Zurich
date: 2023-12-01
"""

from datetime import datetime
import os
import pandas as pd
import numpy as np

from feos.si import *

from Read_input import Read_input
from get_name_properties import add_adsorbent_properties
from psa_process_variable_dependent import psa_process_variable_dependent
from scaling import read_bounds
import multiprocessing


#%% Inputs

# input toml file for general parameters of the process valid for all adsorbents
input_file_toml = 'RSM0097_WIHDUN_cement70.toml'

# input csv file for 1D-DFT parameters of adsorbents
input_file_csv = 'prisma-isotherm-simulated_rsm0011-4602_DFT128fit_max_purity_20231213_094021_sorted_top47.csv'

path_input_directory = 'inputs/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70'
path_results_directory = 'results/screening_loop/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70'

path_bounds = 'inputs/optimization_input/bounds_loop_test.toml'

# determine if full cycle should be calculated or only blowdown. If False, recovery can't be calculated
full_cycle = True

# variables to be updated
variables_to_be_updated = {'p_des': True, 'specific_surface_area': False, 'sigma': False, 'epsilon': False, 'rho': False, 'pore_size': False}
# number of iterations
n_loop = 50

# number of cores to be used for parallelization
n_cores = 23

#%% End Inputs

#%% Input processing

# timestamp for result directory
timestamp_start = datetime.now().strftime("%Y%m%d_%H%M%S")

# create result directory
path_results_directory = f'{path_results_directory}_{timestamp_start}'
os.mkdir(path_results_directory)

path_input_file_toml = path_input_directory + '/' + input_file_toml
path_input_file_csv = path_input_directory + '/' + input_file_csv

# copy input files to result directory
os.system(f'cp {path_input_file_toml} {path_results_directory}/{input_file_toml}')
os.system(f'cp {path_input_file_csv} {path_results_directory}/{input_file_csv}')

# read input csv file
data_csv = pd.read_csv(path_input_file_csv)

# add CCDC code, density, and heat capacity to dataframe of adsorbents
#data_csv = add_adsorbent_properties(data_csv, path_codes, path_properties)

# get indices of all entries in the csv file
indices = list(range(len(data_csv)))

# read bounds
bounds = read_bounds(path_bounds)
x_lb = bounds['p_des']['lower']*PASCAL
x_ub = bounds['p_des']['upper']*PASCAL
x = SIArray1.linspace(x_lb, x_ub, n_loop)


# loop over every entry in the csv file as one row belongs to one adsorbent
def process_adsorbent(i):
    timestamp_start_i = datetime.now().strftime("%Y%m%d_%H%M%S")
    print(f'MOF {i} of {len(indices)}')
    input_data_i = Read_input(path_input_file_toml, data_csv, i)
    adsorbent = input_data_i.create_adsorbent()
    process_conditions = input_data_i.create_process()
    feed_mixture = input_data_i.create_feed_mixture()

    result_data_i = []

    # create folder for results of current adsorbent
    path_results_directory_i = f'{path_results_directory}/{i}_{data_csv["MOF"][i]}_{data_csv["CCDC"][i]}_{timestamp_start_i}'
    os.mkdir(path_results_directory_i)

    # run process model
    for j, x_j in enumerate(x):
        print(f'.....p_des {j} of {len(x)}')
        
        variables_values = {'p_des': x_j}

        try:
            specific_work_in_kJ_kg, purity, df_process_results, df_blowdown, df_repressurization = psa_process_variable_dependent(variables_values, variables_to_be_updated, process_conditions, adsorbent, feed_mixture, full_cycle=full_cycle)
            
            success = True

            working_capacity = df_process_results['working_capacity_in_mol/kg'].iloc[-1]

            if full_cycle:
                recovery = df_process_results['recovery'].iloc[-1]
            else:
                recovery = 0

            max_error_balance_heavy_bd = np.max(np.abs(df_blowdown['balance_heavy_error']))
            max_error_balance_light_bd = np.max(np.abs(df_blowdown['balance_light_error']))
            max_error_balance_energy_bd = np.max(np.abs(df_blowdown['balance_energy_error']))

            max_error_balance_heavy_rp = np.max(np.abs(df_repressurization['balance_heavy_error']))
            max_error_balance_light_rp = np.max(np.abs(df_repressurization['balance_light_error']))
            max_error_balance_energy_rp = np.max(np.abs(df_repressurization['balance_energy_error']))

        except:
            success = False
            df_process_results = pd.DataFrame()
            df_blowdown = pd.DataFrame()
            df_repressurization = pd.DataFrame()
            specific_work_in_kJ_kg = 0
            purity = 0
            recovery = 0
            working_capacity = 0
            max_error_balance_heavy_bd = 0
            max_error_balance_light_bd = 0
            max_error_balance_energy_bd = 0
            max_error_balance_heavy_rp = 0
            max_error_balance_light_rp = 0
            max_error_balance_energy_rp = 0


        result_data_i.append({'p_des_in_Pa': x_j/PASCAL, 'specific_work_in_kJ_kg': specific_work_in_kJ_kg, 'purity': purity, 'recovery': recovery, 'working_capacity_in_mol/kg': working_capacity, 'success': success, 'max_error_balance_heavy_bd': max_error_balance_heavy_bd, 'max_error_balance_light_bd': max_error_balance_light_bd, 'max_error_balance_energy_bd': max_error_balance_energy_bd, 'max_error_balance_heavy_rp': max_error_balance_heavy_rp, 'max_error_balance_light_rp': max_error_balance_light_rp, 'max_error_balance_energy_rp': max_error_balance_energy_rp})

        # save dataframes to csv
        if success:
            df_process_results.to_csv(f'{path_results_directory_i}/{j}_{i}_{data_csv["MOF"][i]}_{data_csv["CCDC"][i]}_df_process_results.csv')
            df_blowdown.to_csv(f'{path_results_directory_i}/{j}_{i}_{data_csv["MOF"][i]}_{data_csv["CCDC"][i]}_df_blowdown.csv')
            if full_cycle:
                df_repressurization.to_csv(f'{path_results_directory_i}/{j}_{i}_{data_csv["MOF"][i]}_{data_csv["CCDC"][i]}_df_repressurization.csv')

        df_results_i = pd.DataFrame(result_data_i)
        df_results_i.to_csv(f'{path_results_directory_i}/{i}_{data_csv["MOF"][i]}_{data_csv["CCDC"][i]}_loop_results_{timestamp_start_i}.csv')

    timestamp_end_i = datetime.now().strftime("%Y%m%d_%H%M%S")

    df_results_i = pd.DataFrame(result_data_i)
    df_results_i.to_csv(f'{path_results_directory_i}/{i}_{data_csv["MOF"][i]}_{data_csv["CCDC"][i]}_loop_results_{timestamp_end_i}.csv')
    df_results_i.to_csv(f'{path_results_directory}/{i}_{data_csv["MOF"][i]}_{data_csv["CCDC"][i]}_loop_results_{timestamp_end_i}.csv')


# Map the process_adsorbent function to each index in parallel
# Create a pool of worker processes with n_cores
pool = multiprocessing.Pool(processes=n_cores)

# Map the process_adsorbent function to each index in parallel
pool.map(process_adsorbent, indices)

# Close the pool and wait for all processes to finish
pool.close()
pool.join()






# %%
