# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering
"""
executable code for optimization of the PSA process model using epsilon-constraint method for multiobjective optimization of specific work and purity or specific work and recovery
author: Fabian Mayer, Energy & Process Systems Engineering, ETH Zurich
date: 2024-02-12
"""

from feos.si import *
import pandas as pd
from datetime import datetime
import os
import numpy as np


from Read_input import Read_input
from knitro_process_optimization import knitro_process_optimization
from psa_process_variable_dependent import psa_process_variable_dependent




#%% Inputs

# input toml file for general parameters of the process valid for all adsorbents
input_file_toml = 'RSM0068_BUSQIQ_cement70.toml'

# input csv file for 1D-DFT parameters of adsorbents
input_file_csv = 'prisma-isotherm-simulated_rsm0011-4602_DFT128fit_max_purity_20240527_131627_sorted_top43.csv'

path_input_directory = 'inputs/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70'
path_results_directory = 'results/opt_eps_constraint/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70_pdes_opt'
results_file_suffix = 'solution_p_des_opt_70'
path_bounds = 'inputs/optimization_input/bounds_general_test.toml'
path_optimization_input = 'inputs/optimization_input/knitro_nlp_test.opt'

# set to True if the respective variable should be optimized or to False if constant value from input_file should be used
variables = {'p_des': True,
            'sigma': False,
            'epsilon': False,
            'rho': False,
            'pore_size': False,
            'specific_surface_area': False}

# give lower and upper value of purity-constraint for epsilon-contraint method. Give same value for lower and upper bound to not use purity in epsilon-constraint method.
min_purity_bounds = [0.7, 0.99]
#min_purity_bounds = [0.0, 0.0]
# give number of single objective optimization problems that should be solved for epsilon-constraint method. Set to 1 to not use purity in epsilon-constraint method. Either n_purity_eps or n_recovery_eps has to be 1 since epsilon-constraint method is only implemented for two objective functions.
n_purity_eps = 30
#n_purity_eps = 10
#n_purity_eps = 1

# give lower and upper value of purity-constraint for epsilon-contraint method. Give same value for lower and upper bound to not use recovery in epsilon-constraint method.
min_recovery_bounds = [0.0, 0.0]
#min_recovery_bounds = [0.7, 0.99]
# give number of single objective optimization problems that should be solved for epsilon-constraint method. Set to 1 to not use recovery in epsilon-constraint method. Either n_purity_eps or n_recovery_eps has to be 1 since epsilon-constraint method is only implemented for two objective functions.
n_recovery_eps = 1
#n_recovery_eps = 30

# CCDC name of adsorbent from csv-file for which the optimization should be performed
ccdc_name = 'BUSQIQ'
ccdc_name_suffix = '_clean'

# set to True if the full process should be calculated for each optimization result
full_process = True

#%% End Inputs

#%% Input processing

# timestamp for result directory
timestamp_start = datetime.now().strftime("%Y%m%d_%H%M%S")

# create result directory
path_results_directory = f'{path_results_directory}_{timestamp_start}'
os.mkdir(path_results_directory)

path_input_file_toml = path_input_directory + '/' + input_file_toml
path_input_file_csv = path_input_directory + '/' + input_file_csv

# create list with constraint values for epsilon-constraint method
min_purity_eps = np.linspace(min_purity_bounds[0], min_purity_bounds[1], num=n_purity_eps)
min_recovery_eps = np.linspace(min_recovery_bounds[0], min_recovery_bounds[1], num=n_recovery_eps)

if n_purity_eps == 1:
    min_purity_or_recovery_eps = min_recovery_eps
elif n_recovery_eps == 1:
    min_purity_or_recovery_eps = min_purity_eps
else:
    raise ValueError('Either n_purity_eps or n_recovery_eps has to be 1 since in this code epsilon-constraint method is only implemented for two objective functions.')


# read input csv file
data_csv = pd.read_csv(path_input_file_csv)

for i, row in data_csv.iterrows():
    if row['CCDC'] == f'{ccdc_name}{ccdc_name_suffix}':
        max_purity = row['max_purity']
        input_data = Read_input(path_input_file_toml, input_dataframe = data_csv, row_csv = i)
        break
else:
    raise ValueError(f'CCDC name {ccdc_name}{ccdc_name_suffix} not found in csv file {path_input_file_csv}')


# create adsorbent object from input data
adsorbent = input_data.create_adsorbent()

# create process conditions object from input data
process_conditions = input_data.create_process()

# create feed mixture object from input data
feed_mixture = input_data.create_feed_mixture()

results = {}

#%% Run optimization


for i, eps in enumerate(min_purity_or_recovery_eps):

    #if n_recovery_eps == 1:
    #    if eps > max_purity:
    #        break

    # create folder for result of single objective optimization problem
    path_results_directory_i = f'{path_results_directory}/{i}'
    os.mkdir(path_results_directory_i)

    if n_recovery_eps == 1:
        min_purity = eps
        min_recovery = min_recovery_eps[0]
        n_eps = n_purity_eps
    elif n_purity_eps == 1:
        min_purity = min_purity_eps[0]
        min_recovery = eps
        n_eps = n_recovery_eps

    solution, specific_work, purity, recovery = knitro_process_optimization(path_optimization_input, process_conditions, adsorbent, feed_mixture, variables, path_bounds, path_results_directory_i, min_purity = min_purity, min_recovery = min_recovery)
    #debugging
    #solution = {}
    #specific_work = eps*1000
    #purity = eps


    print(f'specific work: {specific_work}')
    print(f'purity: {purity}')

    print(f'optimization {i+1} of {n_eps} done')

    # add specific work and purity to solution dictionary
    solution['specific_work_in_kJ/kg'] = specific_work/(KILO*JOULE/KILOGRAM)
    solution['purity'] = purity
    solution['recovery'] = recovery

    timestamp_end = datetime.now().strftime("%Y%m%d_%H%M%S")

    # save solution dictionary to csv
    df_solution = pd.DataFrame.from_dict(solution, orient='index', columns=['value'])
    df_solution.to_csv(f'{path_results_directory_i}/{ccdc_name}_{results_file_suffix}_{timestamp_end}.csv')


    if full_process == True and recovery == 0.0:
        # recalculate the full process with the result from the optimization to get recovery if it is not already calculated

        p_des = solution['p_des_in_Pa']*PASCAL

        variables_values = {'p_des': p_des}

        variables_to_be_updated = {'p_des': True,
                                'sigma': False,
                                'epsilon': False,
                                'rho': False,
                                'pore_size': False,
                                'specific_surface_area': False}

        try:
            specific_work_in_kJ_kg, purity, df_process_results, df_blowdown, df_repressurization = psa_process_variable_dependent(variables_values, variables_to_be_updated, process_conditions, adsorbent, feed_mixture, full_cycle=True)
            
            success = True
            recovery = df_process_results['recovery'].iloc[-1]

        except:
            success = False



    # add results to results dictionary
    if n_recovery_eps == 1:
        results[i] = {'purity_eps': eps, 'purity': purity, 'recovery': recovery, 'specific_work_in_kJ/kg': specific_work/(KILO*JOULE/KILOGRAM)}
    elif n_purity_eps == 1:
        results[i] = {'recovery_eps': eps, 'purity': purity, 'recovery': recovery, 'specific_work_in_kJ/kg': specific_work/(KILO*JOULE/KILOGRAM)}
    else:
        raise ValueError('Either n_purity_eps or n_recovery_eps has to be 1 since in this code epsilon-constraint method is only implemented for two objective functions.')

    # save results dictionary to csv
    df_results = pd.DataFrame.from_dict(results, orient='index')
    df_results.to_csv(f'{path_results_directory}/{ccdc_name}_{results_file_suffix}_eps_constraint.csv')



# save input file to result directory
os.system(f'cp {path_input_file_toml} {path_results_directory}/{input_file_toml}')
os.system(f'cp {path_input_file_csv} {path_results_directory}/{input_file_csv}')

