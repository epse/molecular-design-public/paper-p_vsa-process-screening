# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering
"""
Process evaluation of a PSA process model based on a desorption pressure and/or 1D-DFT parameters from input.
author: Fabian Mayer, Energy & Process Systems Engineering, ETH Zurich
date: 2023-11-14
"""

from feos.si import *

from psa_process_leastsquares import psa_process


def psa_process_variable_dependent(variables_values, variables_to_be_updated, process_conditions, adsorbent, feed_mixture, full_cycle=False):
    """
    Function to evaluate the PSA process model for a given desorption pressure and/or given 1D-DFT parameters.
    The function updates the variables in the adsorbent and process conditions objects and runs the process model.

    Parameters
    ----------
    variables_values: dict of SI floats or unitless floats
        Dictionary containing the values of the variables to be updated.
    variables_to_be_updated: dict of booleans
        Dictionary containing booleans if the variables should be updated for the process evaluation.
    process_conditions: ProcessConditions object
        Object containing the process conditions.
    adsorbent: Adsorbent object
        Object containing the adsorbent properties.
    feed_mixture: FeedMixture object
        Object containing the feed mixture properties.
    full_cycle: bool
        If True, the full cycle is evaluated, if False, only the blowdown is evaluated.

    Returns
    -------
    specific_work_in_kJ/kg: float
        Specific work of the process in kJ/kg.
    purity: float
        Purity of the product.
    df_process_results: pandas dataframe
        Dataframe containing the final results of the process evaluation including process objectives.
    df_blowdown: pandas dataframe
        Dataframe containing the results of the blowdown steps.
    df_repressurization: pandas dataframe
        Dataframe containing the results of the repressurization steps.
    """

    # update variables
    dft_updated = False
    for key, value in variables_to_be_updated.items():
        if key == 'p_des' and value == True:
            process_conditions.update_p_desorption(variables_values[key])
        elif key == 'specific_surface_area' and value == True:
            adsorbent.update_specific_surface_area(variables_values[key])
        elif key in ['sigma', 'epsilon', 'rho', 'pore_size'] and value == True and not dft_updated:
            adsorbent.update_dft_parameters(variables_values['sigma'], variables_values['epsilon'], variables_values['rho'], variables_values['pore_size'])
            dft_updated = True


    # run process model
    df_process_results, df_blowdown, df_repressurization = psa_process(process_conditions, adsorbent, feed_mixture, full_cycle)

    # get specific work
    specific_work_in_kJ_kg = df_process_results['specific_work_in_kJ/kg'].iloc[-1]
    # get purity
    purity = df_process_results['purity'].iloc[-1]


    return specific_work_in_kJ_kg, purity, df_process_results, df_blowdown, df_repressurization
    