# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

from feos.si import *

class ProcessConditions:
    """
    Class to define the process conditions.

    Attributes
    -----------
    temperature_feed: SI float
        Temperature of the feed.
    pressure_feed: SI float
        Pressure of the feed.
    pressure_desorption: SI float
        Final pressure of the desorption.
    steps: int
        Number of calculation steps in the evalution PSA process.
    """

    def __init__(self, temperature_feed, pressure_feed, pressure_desorption, isentropic_efficiency, steps):

        self.temperature_feed = temperature_feed
        self.pressure_feed = pressure_feed
        self.pressure_desorption = pressure_desorption
        self.isentropic_efficiency = isentropic_efficiency
        self.steps = steps

        # array with pressure steps for blowdown and repressurization
        self.p_steps_bd = SIArray1.linspace(pressure_feed, pressure_desorption, steps+1)
        self.p_steps_rp = SIArray1.linspace(pressure_desorption, pressure_feed, steps+1)

    def update_p_desorption(self, p_des):
        """
        Update the desorption pressure.

        Parameters
        ----------
        p_des: SI float
            New desorption pressure.
        """
        self.pressure_desorption = p_des
        self.p_steps_bd = SIArray1.linspace(self.pressure_feed, self.pressure_desorption, self.steps+1)
        self.p_steps_rp = SIArray1.linspace(self.pressure_desorption, self.pressure_feed, self.steps+1)