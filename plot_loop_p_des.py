# ©2024 ETH Zurich, Fabian Mayer; D-MAVT; Energy and Process Systems Engineering

import os
import pandas as pd
import matplotlib.pyplot as plt

# Directory containing the folders
path_results_directory = 'results/screening_loop/01_PrISMa_Adsorption_Isotherms_HoA_sim_simul_spherical_SSE_70_20240119_171839'



# Loop over folders in the directory
for file in os.listdir(path_results_directory):
    file_path = os.path.join(path_results_directory, file)
    # Check if the item in the directory is a file
    if os.path.isfile(file_path) and file.startswith(tuple('0123456789')):
        # Read the CSV file using pandas
        df = pd.read_csv(file_path)

        #plt.scatter(df['p_des_in_Pa']/1000, df['specific_work_in_kJ_kg'])
        plt.scatter(df['p_des_in_Pa']/1000, df['purity'],)
    
#plt.savefig(f'{path_results_directory}/specific_work_vs_p_des.pdf')
        plt.savefig(f'{path_results_directory}/purity_vs_p_des.pdf')
        
        


  